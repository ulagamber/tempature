package com.jamaica.temperature

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.jamaica.temperature.utils.NetworkUtils
import com.jamaica.temperature.utils.ProgressBarHD
import com.valdesekamdem.library.mdtoast.MDToast

abstract class BaseActivity : AppCompatActivity()  , IBaseView {

    private var cProgressDialog: ProgressBarHD? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
if(isNetworkConnected()){
    setContentView(getLayoutResId())
}else{
setContentView(R.layout.no_internet_connect)
}
    }

    @LayoutRes
    protected abstract fun getLayoutResId(): Int

    override fun showStatus(message: String, type: Int) {
        MDToast.makeText(this, message, MDToast.LENGTH_SHORT, type).show()
    }

    override fun showMessage(message: String) {
        MDToast.makeText(this, message, MDToast.LENGTH_SHORT, MDToast.TYPE_SUCCESS).show()
    }

    override fun isNetworkConnected(): Boolean {
        return NetworkUtils.isNetWorkAvailable(this)
    }

    override fun showProgressBar() {
        if (!isFinishing) progressDialog().show()
    }

    override fun dismissProgressBar() {
        if (!isFinishing && cProgressDialog != null && cProgressDialog?.isShowing()!!) cProgressDialog?.dismiss()
    }

    private fun progressDialog(): ProgressBarHD {
        if (cProgressDialog == null) {
            cProgressDialog = ProgressBarHD(this)
        }
        return cProgressDialog!!
    }
}