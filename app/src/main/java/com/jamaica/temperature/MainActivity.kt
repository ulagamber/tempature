package com.jamaica.temperature

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.jamaica.temperature.service.AsyncTaskCompleteListener
import com.jamaica.temperature.utils.Constants
import com.jamaica.temperature.utils.NetworkUtils
import com.jamaica.temperature.utils.ProgressBarHD

class MainActivity : AppCompatActivity(){

    private var cProgressDialog: ProgressBarHD? = null
    private var pref: SharedPreferences? = null
    var main:MainActivity? = null

    fun getInstatnce() : MainActivity?{
        return main
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
main = this
        pref = getSharedPreferences(Constants.PrefName, Context.MODE_PRIVATE)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_history, R.id.navigation_analytics,R.id.navigation_menu
            )
        )
       // setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


    }

    fun isNetworkConnected(): Boolean {
        return NetworkUtils.isNetWorkAvailable(this)
    }

    fun showProgressBar() {
        if (!isFinishing) progressDialog().show()
    }

    fun dismissProgressBar() {
        if (!isFinishing && cProgressDialog != null && cProgressDialog?.isShowing()!!) cProgressDialog?.dismiss()
    }

    private fun progressDialog(): ProgressBarHD {
        if (cProgressDialog == null) {
            cProgressDialog = ProgressBarHD(this)
        }
        return cProgressDialog!!
    }
}
