package com.jamaica.temperature.extensions

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.util.Patterns
import android.widget.Toast
import androidx.core.content.ContextCompat

import com.valdesekamdem.library.mdtoast.MDToast
import java.text.DecimalFormat
import java.util.regex.Pattern
import com.google.gson.Gson
import com.jamaica.temperature.utils.Constants


fun String.isValidEmail(): Boolean =
    this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun isValidEmail(email: CharSequence?): Boolean =
    email != null && Patterns.EMAIL_ADDRESS.matcher(email).matches()

fun isValidWebsite(website: CharSequence?): Boolean =
    website != null && Patterns.WEB_URL.matcher(website).matches()

fun isNullOrEmpty(value: String?): Boolean =
    value == null || value.isEmpty() || value.equals("null", ignoreCase = true)

fun isMerchantId(value: Int): Boolean = value in 24 downTo 4

fun isValidAccountNo(value: Int): Boolean = value in 20 downTo 12

fun isUsernameValid(value: String): Boolean = Pattern.matches("^[a-z,0-9]", value)

fun isUsername(value: Int): Boolean = value in 64 downTo 6

fun isBusinessName(value: Int): Boolean = value in 56 downTo 5

fun isBrandName(value: Int): Boolean = value in 128 downTo 3

fun isPassword(value: Int): Boolean = value in 24 downTo 6

fun isLandline(value: Int): Boolean = value == 7

fun isFirstName(value: Int): Boolean = value in 128 downTo 3

fun isPhoneNumber(value: Int): Boolean = value in 10 downTo 7

fun isOfferName(value: Int): Boolean = value in 25 downTo 3

fun isOfferDesc(value: Int): Boolean = value in 512 downTo 3

fun isOfferCode(value: Int): Boolean = value in 10 downTo 4

fun isOfferMinTransactionAmt(value: Int): Boolean = value in 1000 downTo 1

fun isOfferMaxDiscAmt(value: Int): Boolean = value in 2000 downTo 0

fun isMaxRedeemPercentage(value: Int): Boolean = value in 2000 downTo 0

fun showToastMessage(context: Context, message: String, duration: Int = Toast.LENGTH_SHORT) =
    Toast.makeText(context, message, duration).show()

fun showErrorMessage(context: Context, message: String, duration: Int = Toast.LENGTH_SHORT) =
    MDToast.makeText(context, message, duration, MDToast.TYPE_ERROR).show()

fun isAmountValid(amount: String?) = amount?.matches(Constants.PRICE_PATTERN.toRegex())

fun roundAmount(amount: String?): String {
    if (isNullOrEmpty(amount) || amount.equals("0", ignoreCase = true)) return "0.0"
    var currencyFormat = ""
    try {
        val dblAmount = java.lang.Double.parseDouble(amount!!)
        val decimalFormat = DecimalFormat(".##")
        //currencyFormat = decimalFormat.format(dblAmount)
        currencyFormat = currencyFormat(decimalFormat.format(dblAmount))
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return currencyFormat
}

fun roundDown(amount: Double?): Double {
    if (amount == null) return 0.0
    if (amount < 0 || amount < 0.0) return 0.0
    return Math.floor(amount)
}

fun currencyFormat(amount: String): String {
    val formatter = DecimalFormat("##,##,##0.00")
    return formatter.format(java.lang.Double.parseDouble(amount))
}

fun <T> serialize(clazz: T): String {
    return Gson().toJson(clazz)
}

fun <T> deSerialize(json: String, clazz: Class<T>): T {
    return Gson().fromJson(json, clazz)
}

fun Int.toBitmap(context: Context): Bitmap? {
    // retrieve the actual drawable
    val drawable = ContextCompat.getDrawable(context, this) ?: return null
    drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
    val bm = Bitmap.createBitmap(
        drawable.intrinsicWidth,
        drawable.intrinsicHeight,
        Bitmap.Config.ARGB_8888
    )
    // draw it onto the bitmap
    val canvas = Canvas(bm)
    drawable.draw(canvas)
    return bm
}
