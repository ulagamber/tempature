package com.jamaica.temperature.extensions

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager


import com.google.android.material.button.MaterialButton
import com.jamaica.temperature.R
import java.util.*


fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun MaterialButton.attributes(enable: Boolean = false) {
    isEnabled = enable
    if (!enable) {
        setBackgroundColor(ContextCompat.getColor(context, R.color.button_disable))
    } else {
        setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
    }
}

fun EditText.onMaxLength(length: Int) {
    val filterArray = arrayOfNulls<InputFilter>(1)
    filterArray[0] = InputFilter.LengthFilter(length)
    filters = filterArray
}


fun MaterialButton.attributeoutline(enable: Boolean = false) {
    isEnabled = enable
    if (!enable) {
        setBackgroundColor(ContextCompat.getColor(context, R.color.shadow))
    } else {
        setBackgroundColor(ContextCompat.getColor(context, R.color.white))
    }
}
fun EditText.afterTextChanged(activity: Activity, action: (Editable) -> Unit) {
    var timer: Timer? = null
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable) {
            timer = Timer()
            timer?.schedule(object : TimerTask() {
                override fun run() {
                    activity.runOnUiThread { action(editable) }
                }
            }, 700)
        }

        override fun beforeTextChanged(string: CharSequence?, start: Int, count: Int, after: Int) =
            Unit

        override fun onTextChanged(string: CharSequence?, start: Int, before: Int, count: Int) {
            if (timer != null) {
                timer?.cancel()
            }
        }
    })
}
fun MaterialButton.Visibility(visible: Boolean = true) {

    if (visible) {

        visibility = View.VISIBLE

    } else {
        visibility = View.GONE

    }
}


fun CheckBox.onChecked(action: (compoundButton: CompoundButton, isChecked: Boolean) -> Unit) {
    setOnCheckedChangeListener { compoundButton, b ->
        action(compoundButton, b)
    }
}

fun EditText.onTextChanged(action: (CharSequence) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(string: Editable?) = Unit
        override fun beforeTextChanged(string: CharSequence?, start: Int, count: Int, after: Int) =
            Unit

        override fun onTextChanged(string: CharSequence?, start: Int, before: Int, count: Int) {
            action(string ?: "")
        }
    })
}

fun EditText.clearOnTextChangedListener() {
    onTextChanged {}
}

fun ViewPager.pageChangeListener(action: (Int) -> Unit) {
    addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) = Unit
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) = Unit

        override fun onPageSelected(position: Int) {
            action(position)
        }
    })
}


fun TextView.Spanningunderline(context: Context, content: String) {
    val spannableString = SpannableString(content)
    spannableString.setSpan(UnderlineSpan(), 0, content.length, 0)
    setText(spannableString)

}


/*fun CheckBox.makeLinks(context: Context, vararg links: Pair<String, PrivacyPolicyListener>) {
    val spannableString = SpannableString(text)
    for (link in links) {
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                view.cancelPendingInputEvents()
                //setPrivacySettingsChecked(view)
                link.second.onPolicyClick(view, link.first)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                ds.color = ContextCompat.getColor(context, R.color.blue)
            }
        }
        val startIndexOfLink = this.text.toString().indexOf(link.first)
        spannableString.setSpan(
            clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    this.movementMethod =
        LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
    setText(spannableString)
}*/

fun SeekBar.seekBarChangeListener(action: (SeekBar, Int, Boolean) -> Unit) {
    setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            action(seekBar, if (progress < 1) progress else 1, fromUser)
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit

        override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
    })
}
/*

fun ImageView.loadUrl(progressBar: ProgressBar?, url: String?) {
    if (isNullOrEmpty(url)) return
    Glide.with(context)
        .load(url)
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                if (progressBar != null) progressBar.visibility = View.GONE
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                if (progressBar != null) progressBar.visibility = View.GONE
                return false
            }
        })
        .into(this)
}

*/
/*fun ImageView.loadSvgUrl(uri: Uri) {
    GlideToVectorYou
        .init()
        .with(context)
        .load(uri, this)
}*//*


fun View.loadBackground(url: String) {
    Glide.with(this).load(url).into(object : CustomTarget<Drawable>() {
        override fun onLoadCleared(placeholder: Drawable?) {
        }

        override fun onResourceReady(
            resource: Drawable,
            transition: com.bumptech.glide.request.transition.Transition<in Drawable>?
        ) {
            background = resource
        }
    })
}
*/
