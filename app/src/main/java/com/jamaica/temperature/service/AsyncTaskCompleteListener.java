package com.jamaica.temperature.service;

public interface AsyncTaskCompleteListener<T> {
    void onTaskComplete(int code,T result);
}
