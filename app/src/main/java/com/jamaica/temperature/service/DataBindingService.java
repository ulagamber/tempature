package com.jamaica.temperature.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;


import com.jamaica.temperature.R;
import com.jamaica.temperature.utils.Constants;
import com.jamaica.temperature.utils.ProgressBarHD;

import java.io.IOException;

public class DataBindingService extends AsyncTask<String, Void, String> {
    private Context mContext;
    private AsyncTaskCompleteListener<String> callback;
    private URL_ConnectionService conn;
    private String response;
//    private ProgressBarHD progressbar;
    private String pro_msg = "",token="";
    private int pro_color;
    private int Method;
    public DataBindingService(Context context, AsyncTaskCompleteListener<String> cb,
                              String url, String params, Integer parseMethod){
        mContext = context;

        callback = cb;
        if(parseMethod==2) {
            conn = new URL_ConnectionService(mContext, url, params);
        }else{
            conn = new URL_ConnectionService(mContext, url);
        }
        pro_color = mContext.getResources().getColor(R.color.colorAccent);
//        progressbar = new ProgressBarHD(mContext);
        Method = parseMethod;
    }
    public DataBindingService(Context context, AsyncTaskCompleteListener<String> cb,
                              String url, String params, Integer parseMethod, String _token) {
        mContext = context;

        callback = cb;
        if(parseMethod==2) {
            conn = new URL_ConnectionService(mContext, url, params);
        }else{
            conn = new URL_ConnectionService(mContext, url);
        }
        pro_color = mContext.getResources().getColor(R.color.colorAccent);
//        progressbar = new ProgressBarHD(mContext);
        Method = parseMethod;
        token = _token;
    }

    public DataBindingService(Context context, AsyncTaskCompleteListener<String> cb, String apiUrl,
                              String params, String progressMsg, int progressColor,  Integer parseMethod) {
        mContext = context;
        callback = cb;
        conn = new URL_ConnectionService(mContext, apiUrl, params);
        pro_color =progressColor;
        pro_msg = progressMsg;
//        progressbar = new ProgressBarHD(mContext);
        Method = parseMethod;
    }
    public DataBindingService(Context context, AsyncTaskCompleteListener<String> cb, String apiUrl,
                              String params, String progressMsg, int progressColor,  Integer parseMethod,String _token) {
        mContext = context;
        callback = cb;
        conn = new URL_ConnectionService(mContext, apiUrl, params);
        pro_color =progressColor;
        pro_msg = progressMsg;
//        progressbar = new ProgressBarHD(mContext);
        Method = parseMethod;
        token = _token;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        progressbar.showDialog(""+pro_msg,false,pro_color);
    }

    @Override
    protected String doInBackground(String... strings) {

        try {
            if(Method==2) {
                response = conn.sendPOST(token);
            }else {
                response = conn.sendGET(token);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
//        progressbar.D_Cancel();
        if(result.length()>0 && result.contains("~")) {
            String[] split = result.split("~");
            if(split.length>1) {
                int code = Integer.parseInt(split[0]);
                callback.onTaskComplete(code, split[1]);
            }
        }
    }
}
