package com.jamaica.temperature.service

import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.io.UnsupportedEncodingException
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.util.ArrayList

import javax.net.ssl.HttpsURLConnection


import android.content.Context
import android.provider.Settings.Secure
import android.util.Log
import com.jamaica.temperature.utils.APIUtils


import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.HttpVersion
import org.apache.http.NameValuePair
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.ResponseHandler
import org.apache.http.client.methods.HttpPost
//import org.apache.http.entity.mime.HttpMultipartMode
//import org.apache.http.entity.mime.MultipartEntity
//import org.apache.http.entity.mime.content.StringBody
import org.apache.http.impl.client.BasicResponseHandler
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.message.BasicNameValuePair
import org.apache.http.params.BasicHttpParams
import org.apache.http.params.CoreProtocolPNames
import org.apache.http.params.HttpParams
import org.apache.http.util.EntityUtils


@Suppress("DEPRECATION")
class PostDataHelper {

    private lateinit var mHttpClient: DefaultHttpClient;
    internal var keys = ArrayList<String>()

    internal var values = ArrayList<String>()

    internal lateinit var _context: Context
//    val mAmberUtils:AmberUtils = AmberUtils()
    private val deviceId: String
        get() = Secure.getString(_context.contentResolver, Secure.ANDROID_ID)

    constructor() {

    }

    constructor(_keysad: ArrayList<String>, _valuesad: ArrayList<String>, ctx: Context) {

        _context = ctx
        /*keys.add("Platform")
        values.add("Android")

        keys.add("DeviceId")
        values.add(mAmberUtils.getAndroidID(_context))

        keys.add("DeviceOffset")
        values.add(""+ (mAmberUtils.getDeviceOffset(_context) / 1000))

        keys.add("DeviceDateTime")
        values.add(mAmberUtils.getDeviceTimeStamp())

        keys.add("DeviceTimezone")
        values.add(mAmberUtils.gettimezone(_context))

        keys.add("Version")
        values.add("3.4")

        keys.add("Appname")
        values.add("AmberConnect")*/

        keys.addAll(_keysad)
        values.addAll(_valuesad)


        val params = BasicHttpParams()
        params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1)
        mHttpClient = DefaultHttpClient(params)

    }

    //	public String postData(String url)
    //	{
    //		// Create a new HttpClient and Post Header
    //		StringBuffer sb = new StringBuffer();
    //		sb.append(url);
    //		sb.append("?");
    //		for (int i = 0; i < keys.size(); i++)
    //		{
    //			if (i != 0)
    //				sb.append("&");
    //			sb.append(keys.get(i));
    //			sb.append("=");
    //			sb.append(values.get(i));
    //
    //		}
    //		AmberUtils.Logcats("Url", sb.toString());
    //
    //		HttpPost httppost = new HttpPost(url);
    //
    //		try
    //		{
    //			// Add your data
    //			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(keys.size());
    //
    //			for (int i = 0; i < keys.size(); i++)
    //			{
    //
    //				nameValuePairs.add(new BasicNameValuePair(keys.get(i), values.get(i)));
    //
    //			}
    //
    //			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
    //
    //			HttpParams httpParameters = new BasicHttpParams();
    //			// Set the timeout in milliseconds until a connection is
    //			// established.
    //			int timeoutConnection = 200000;
    //
    //			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
    //			// Set the default socket timeout (SO_TIMEOUT)
    //			// in milliseconds which is the timeout for waiting for data.
    //			int timeoutSocket = 200000;
    //
    //			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
    //			HttpConnectionParams.setTcpNoDelay(httpParameters, true);
    //
    //			DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
    //
    //			BasicHttpResponse httpResponse = (BasicHttpResponse) httpClient.execute(httppost);
    //
    //			StatusLine statusLine = httpResponse.getStatusLine();
    //
    //			if (statusLine.getStatusCode() >= 300)
    //			{
    //
    //				return null;
    //
    //			}
    //
    //			HttpEntity entity = httpResponse.getEntity();
    //
    //			if (entity == null)
    //			{
    //
    //			}
    //
    //			return entity == null ? null : EntityUtils.toString(entity);
    //
    //		}
    //		catch (ClientProtocolException e)
    //		{
    //			e.printStackTrace();
    //		}
    //		catch (IOException e)
    //		{
    //			e.printStackTrace();
    //		}
    //		catch (Exception e)
    //		{
    //			e.printStackTrace();
    //		}
    //		return null;
    //
    //	}

/*
// kotlin conversion not working this function

    fun postData(url1: String): String {

        mAmberUtils.Logcats("UrlTest", "Url dashboard_middle_lay:$url1?")

        var response = ""
        try {
            val httppost = HttpPost("$url1?")
            val multipartEntity = MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE)
            for (i in keys.indices) {
                multipartEntity.addPart("" + keys[i], StringBody("" + values[i]))
                mAmberUtils.Logcats("UrlTest", "Url dashboard_middle_lay:" + keys[i] + "=" + values[i])
            }


            httppost.entity = multipartEntity
            val responseHandler = PhotoUploadResponseHandler()
            response = mHttpClient.execute(httppost, responseHandler)


        } catch (e: Exception) {
            Log.e("TAG", "dashboard data eroror:" + e.localizedMessage, e)
        }

        return response
    }
*/

    fun postData(requestURL: String): String {

        val sb = StringBuffer()
        sb.append(APIUtils.BASE_URL+requestURL)
        sb.append("?")
        for (i in keys.indices) {
            if (i != 0)
                sb.append("&")
            sb.append(keys[i])
            sb.append("=")
            sb.append(values[i])

        }
        Log.d("UrlTest", "UrlString"+sb.toString())


        val url: URL
        var response = ""
        try {

            url = URL(APIUtils.BASE_URL+requestURL)

            val conn = url.openConnection() as HttpURLConnection
            conn.readTimeout = 180000
            conn.connectTimeout = 180000
            conn.requestMethod = "POST"
            conn.doInput = true
            conn.doOutput = true


            val os = conn.outputStream
            val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
            writer.write(getPostDataString(keys, values))

            writer.flush()
            writer.close()
            os.close()
            val responseCode = conn.responseCode

            Log.d("UrlTest", "responseCode:"+responseCode)

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                 var line: String? = null
                 val br = BufferedReader(InputStreamReader(conn.inputStream))
                 while ((line == br.readLine()) != null) {
                     response += line
                 }
//                val reader: BufferedReader = BufferedReader(InputStreamReader(conn.inputStream))
//                response = reader.readLine()

            } else {
                response = ""

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return response
    }

    fun postDataLang(requestURL: String): String {

        val sb = StringBuffer()
        sb.append(requestURL)
        sb.append("?")
        for (i in keys.indices) {
            if (i != 0)
                sb.append("&")
            sb.append(keys[i])
            sb.append("=")
            sb.append(values[i])

        }
//        mAmberUtils.Logcats("UrlTest", sb.toString())


        val url: URL
        var response = ""
        try {
            url = URL(requestURL)

            val conn = url.openConnection() as HttpURLConnection
            conn.readTimeout = 180000
            conn.connectTimeout = 180000
            conn.requestMethod = "POST"
            conn.doInput = true
            conn.doOutput = true


            val os = conn.outputStream
            val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
            writer.write(getPostDataString(keys, values))

            writer.flush()
            writer.close()
            os.close()
            val responseCode = conn.responseCode

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                /* var line: String? = null
                 val br = BufferedReader(InputStreamReader(conn.inputStream))
                 while ((line == br.readLine()) != null) {
                     response += line
                 }*/
                val reader: BufferedReader = BufferedReader(InputStreamReader(conn.inputStream))
                response = reader.readLine()

            } else {
                response = ""

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return response
    }

    private inner class PhotoUploadResponseHandler : ResponseHandler<String> {

        @Throws(ClientProtocolException::class, IOException::class)
        override fun handleResponse(response: HttpResponse): String {

            val r_entity = response.entity
            val responseString = EntityUtils.toString(r_entity)
            Log.d("UPLOAD", "response Striing :$responseString")

            return responseString
        }

    }

    fun postDataReminder(requestURL: String): String {

        val sb = StringBuffer()
        sb.append(requestURL)
        sb.append("?")
        for (i in keys.indices) {
            if (i != 0)
                sb.append("&")
            sb.append(keys[i])
            sb.append("=")
            sb.append(values[i])

        }
//        mAmberUtils.Logcats("UrlTest", sb.toString())


        val url: URL
        var response = ""
        try {
            url = URL(requestURL)

            val conn = url.openConnection() as HttpURLConnection
            conn.readTimeout = 180000
            conn.connectTimeout = 180000
            conn.requestMethod = "POST"
            conn.doInput = true
            conn.doOutput = true


            val os = conn.outputStream
            val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
            writer.write(getPostDataString(keys, values))

            writer.flush()
            writer.close()
            os.close()
            val responseCode = conn.responseCode

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                /*  var line: String? = null
                  val br = BufferedReader(InputStreamReader(conn.inputStream))
                  while ((line == br.readLine()) != null) {
                      response += line
                  }*/

                val reader: BufferedReader = BufferedReader(InputStreamReader(conn.inputStream))
                response = reader.readLine()

            } else {
                response = ""

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return response
    }


    fun postDataNew(requestURL: String): String {

//        		mAmberUtils.Logcats("UrlTest", "Url post data new:"+requestURL);


        val url: URL
        var response = ""
        try {
            url = URL(requestURL)


            val conn = url.openConnection() as HttpURLConnection
            conn.readTimeout = 60000
            conn.connectTimeout = 120000
            conn.requestMethod = "POST"
            conn.doInput = true
            conn.doOutput = true

            val params = ArrayList<NameValuePair>()

            val sb = StringBuffer()
            sb.append(requestURL)
            sb.append("?")
            for (i in keys.indices) {
                if (i != 0)
                    sb.append("&")
                sb.append(keys[i])
                sb.append("=")
                sb.append(values[i])
                params.add(BasicNameValuePair(keys[i], values[i]))
            }

//            mAmberUtils.Logcats("UrlTest", "Url post data new380:"+sb.toString());
            //            Log.d("paramsofpostda",params.toString());

            val os = conn.outputStream
            val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
            writer.write(getQuery(params))
//            mAmberUtils.Logcats("UrlTest", "Url post data new:378")
            writer.flush()
            writer.close()
            os.close()
            val responseCode = conn.responseCode
//            mAmberUtils.Logcats("UrlTest", "Url post data new:383 result code:"+responseCode)
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                /* var line: String?=null
 //                val br = BufferedReader(InputStreamReader(conn.inputStream))

                 mAmberUtils.Logcats("UrlTest", "Url post data new:388 result code:"+responseCode)
                 conn.inputStream.bufferedReader().use { reader ->
                     // ... do something with the reader
                     mAmberUtils.Logcats("UrlTest", "Url post data new:391 result code:"+responseCode)
                     while ((line == reader.readLine()) != null) {
                         mAmberUtils.Logcats("UrlTest", "Url post data new:383 result code393:"+responseCode)
                         response += line
                     }
                 }*/

                val reader: BufferedReader = BufferedReader(InputStreamReader(conn.inputStream))
                response = reader.readLine()

//                mAmberUtils.Logcats("UrlTest", "Url post data new: resposne390;"+response)
            } else {
                response = ""
            }
        } catch (e: Exception) {
            e.printStackTrace()
//            mAmberUtils.Logcats("UrlTest", "Url post data new: error;"+e.message)
        }
//        mAmberUtils.Logcats("UrlTest", "Url post data new: resposne;"+response)
        return response


    }

    @Throws(UnsupportedEncodingException::class)
    private fun getPostDataString(keys: ArrayList<String>, values: ArrayList<String>): String {
        val result = StringBuilder()
        var first = true
        // for(Map.Entry<String, String> entry : params.entrySet()){
        // if (first)
        // first = false;
        // else
        // result.append("&");
        //
        // result.append(URLEncoder.encode(keys.get(i), "UTF-8"));
        // result.append("=");
        // result.append(URLEncoder.encode(values.get(i), "UTF-8"));
        // }

        for (i in keys.indices) {

            if (first)
                first = false
            else
                result.append("&")

            result.append(URLEncoder.encode(keys[i], "UTF-8"))
            result.append("=")
            result.append(URLEncoder.encode(values[i], "UTF-8"))

        }

        return result.toString()
    }

    @Throws(UnsupportedEncodingException::class)
    private fun getQuery(params: List<NameValuePair>): String {
        val result = StringBuilder()
        var first = true

        for (pair in params) {
            if (first)
                first = false
            else
                result.append("&")

            result.append(URLEncoder.encode(pair.name, "UTF-8"))
            result.append("=")
            result.append(URLEncoder.encode(pair.value, "UTF-8"))
        }

        return result.toString()
    }

}
