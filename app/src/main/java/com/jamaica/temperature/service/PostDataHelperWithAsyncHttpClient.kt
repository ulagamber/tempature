package com.jamaica.temperature.service

import java.util.ArrayList

import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair



import android.content.Context
import android.util.Log
import com.jamaica.temperature.utils.APIUtils
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams

class PostDataHelperWithAsyncHttpClient {

    private val client = AsyncHttpClient()

    fun cancelMyRequest(ct: Context, cancelStatus: Boolean) {
        client.cancelRequests(ct, cancelStatus)
    }

    fun post(
        _keys: ArrayList<String>, _values: ArrayList<String>,
        ctx: Context, url: String, token:String, responseHandler: AsyncHttpResponseHandler
    ) {

        val keys = ArrayList<String>()
        val values = ArrayList<String>()

        keys.addAll(_keys)
        values.addAll(_values)

        val sb = StringBuffer()
        sb.append(APIUtils.BASE_URL+url)
        sb.append("?")
        for (i in keys.indices) {
            if (i != 0)
                sb.append("&")
            sb.append(keys[i])
            sb.append("=")
            sb.append(values[i])

        }

        Log.d("Url", sb.toString())

        val params = RequestParams()
        val nameValuePairs = ArrayList<NameValuePair>(
            keys.size
        )

        for (i in keys.indices) {
            params.put(keys[i], values[i])
            nameValuePairs.add(
                BasicNameValuePair(
                    keys[i], values[i]
                )
            )
        }

        client.setTimeout(200000)
client.addHeader("Authorization", ""+token);
        client.post(APIUtils.BASE_URL+url, params, responseHandler)

    }

}