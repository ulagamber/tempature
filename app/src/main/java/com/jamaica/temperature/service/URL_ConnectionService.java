package com.jamaica.temperature.service;

import android.content.Context;
import android.util.Log;

import com.jamaica.temperature.utils.APIUtils;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

public class URL_ConnectionService {

    private Context mContext;
    private String GET_URL;
    private String jsonParams;
private String res = "";

    /*
    * JSONObject postDataParams = new JSONObject();
            postDataParams.put("name", "abc");
            postDataParams.put("email", "abc@gmail.com");
    * */
    public URL_ConnectionService(Context context, String URL, String params){
        mContext = context;
        GET_URL = APIUtils.BASE_URL+URL;
        jsonParams = params;
    }
    public URL_ConnectionService(Context context, String URL){
        mContext = context;
        GET_URL = APIUtils.BASE_URL+URL;
        Log.d("tag","API url connection:"+GET_URL);
    }

    public String sendGET(String token) throws IOException {
        URL obj = new URL(""+GET_URL);
        Log.d("tag","API url connection48:"+GET_URL);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
//        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Authorization", ""+token);
        con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        int responseCode = con.getResponseCode();
        System.out.println("GET Response Code :: " + responseCode);
        Log.d("tag","API url connection 55:"+responseCode);
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            System.out.println(response.toString());
            String repos = response.toString();
            /*if(responseCode==200) {
                res.Success(repos);
            }else{
                res.Failure(repos);
            }*/
res = response.toString();
            return responseCode+"~"+res;
        } else {
            System.out.println("GET request not worked");
            return responseCode+"~"+res;
        }

    }

    public String sendPOST(String token) throws IOException {
        URL obj = new URL(GET_URL);
        System.out.println("aar lib response line77:"+"\n"+GET_URL+"\n"+jsonParams);
        try {
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", ""+token);
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(jsonParams);
            writer.flush();
            writer.close();
            os.close();
            // For POST only - END

            int responseCode = con.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                System.out.println("aar lib response:"+response.toString());

                String repos = response.toString();
                /*if(responseCode==200) {
                    res.Success(repos);
                }else{
                    res.Failure(repos);
                }*/
                System.out.println("POST request worked Res:"+response.toString());
                res = response.toString();
                return responseCode+"~"+res;
            } else {
                System.out.println("GET request not worked");
                return responseCode+"~"+res;
            }

        }catch (Exception e){
            System.out.println("aar lib response line131 error:"+e.getMessage());
        }
        return "";
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}
