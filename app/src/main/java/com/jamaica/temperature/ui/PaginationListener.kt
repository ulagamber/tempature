package com.jamaica.temperature.ui

import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class PaginationListener(var layoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {
    val PAGE_START = 1
    val PAGE_SIZE = 10
    /*@NonNull
    var layoutManager: LinearLayoutManager? = null

    *//**
     * Set scrolling threshold here (for now i'm assuming 10 item in one page)
     *//*


    *//**
     * Supporting only LinearLayoutManager for now.
     *//*
    fun PaginationListener(@NonNull _layoutManager: LinearLayoutManager?) {
        this.layoutManager = _layoutManager
    }*/

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

            val visibleItemCount: Int = layoutManager!!.getChildCount()
            val totalItemCount: Int = layoutManager!!.getItemCount()
            val firstVisibleItemPosition: Int = layoutManager!!.findFirstVisibleItemPosition()
            if (!isLoading() && !isLastPage()) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE
                ) {
                    loadMoreItems()
                }
            }

    }

    abstract fun loadMoreItems()
    abstract fun isLastPage(): Boolean
    abstract fun isLoading(): Boolean
}