package com.jamaica.temperature.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.jamaica.temperature.BaseActivity
import com.jamaica.temperature.IBaseView
import com.jamaica.temperature.MainActivity
import com.jamaica.temperature.R
import com.jamaica.temperature.service.AsyncTaskCompleteListener
import com.jamaica.temperature.service.DataBindingService
import com.jamaica.temperature.ui.data.CountryCodeData
import com.jamaica.temperature.ui.data.RangeData
import com.jamaica.temperature.ui.login.LoginActivity
import com.jamaica.temperature.utils.*
import org.json.JSONObject

class SplashActivity : AppCompatActivity(), AsyncTaskCompleteListener<String> {

    private var pref: SharedPreferences? = null
    private var cProgressDialog: ProgressBarHD? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pref = getSharedPreferences(Constants.PrefName, Context.MODE_PRIVATE)

        Commonutils.saveBoolean(pref!!,Constants.CELSIUS_OR_TEMP,false)

        if (isNetworkConnected()) {
            setContentView(R.layout.layout_splash)
        } else {
            setContentView(R.layout.no_internet_connect)
        }


    }

    override fun onResume() {
        super.onResume()
        showProgressBar()
        API_parse()

    }

    fun isNetworkConnected(): Boolean {
        return NetworkUtils.isNetWorkAvailable(this)
    }

    fun showProgressBar() {
        if (!isFinishing) progressDialog().show()
    }

    fun dismissProgressBar() {
        if (!isFinishing && cProgressDialog != null && cProgressDialog?.isShowing()!!) cProgressDialog?.dismiss()
    }

    private fun progressDialog(): ProgressBarHD {
        if (cProgressDialog == null) {
            cProgressDialog = ProgressBarHD(this)
        }
        return cProgressDialog!!
    }

    fun API_parse() {
        DataBindingService(
            applicationContext,
            this,
            APIUtils.CORE_CONFIG + "?" + APIUtils.USER_TYPE + "=3",
            "",
            APIUtils.GET
        ).execute("")
    }

    override fun onTaskComplete(code:Int, result: String?) {
        Log.d("tag", "Config resposne:" + result)
        var json = JSONObject(result)
        var data = Commonutils.hasJsonForKey(json, "data")

        var currencycode = Commonutils.hasStringForKey(data, "default_currency")

        var temp_start = Commonutils.hasStringForKey(
            Commonutils.hasJsonForKey(data, "temperature"),
            "stat_value"
        )
        var temp_end =
            Commonutils.hasStringForKey(Commonutils.hasJsonForKey(data, "temperature"), "end_value")
        var cels_start =
            Commonutils.hasStringForKey(Commonutils.hasJsonForKey(data, "celsius"), "stat_value")
        var cels_end =
            Commonutils.hasStringForKey(Commonutils.hasJsonForKey(data, "celsius"), "end_value")
        var high_temp = Commonutils.hasStringForKey(
            Commonutils.hasJsonForKey(data, "high_alert"),
            "temperature"
        )
        var high_cels =
            Commonutils.hasStringForKey(Commonutils.hasJsonForKey(data, "high_alert"), "celsius")

        Commonutils.saveString(pref!!, Constants.CURRENCY_CODE, currencycode)
        Commonutils.saveString(pref!!, Constants.TEMP_START, temp_start)
        Commonutils.saveString(pref!!, Constants.TEMP_END, temp_end)
        Commonutils.saveString(pref!!, Constants.CELSIUS_START, cels_start)
        Commonutils.saveString(pref!!, Constants.CELSIUS_END, cels_end)
        Commonutils.saveString(pref!!, Constants.HIGH_CELSIUS, high_cels)
        Commonutils.saveString(pref!!, Constants.HIGH_TEMP, high_temp)

        var countryCodeList = Commonutils.hasArrayForKey(data, "dialing_codes")
        var symptomList = Commonutils.hasArrayForKey(data, "symptoms")
        var genderList = Commonutils.hasArrayForKey(data, "gender")
        var rangeList = Commonutils.hasArrayForKey(data, "range_guide")

        for (i in 0 until countryCodeList.length()) {
            var obj = countryCodeList.getJSONObject(i)
            var cdata = CountryCodeData(
                Commonutils.hasStringForKey(obj, "id"), Commonutils.hasStringForKey(obj, "code"),
                Commonutils.hasStringForKey(obj, "country_name")
            )
            Constants.countrycodelist.add(cdata)
        }
        for (i in 0 until symptomList.length()) {
            var obj = symptomList.getJSONObject(i)
            var cdata = CountryCodeData(
                Commonutils.hasStringForKey(obj, "id"),
                Commonutils.hasStringForKey(obj, "name")
            )
            Constants.symptomlist.add(cdata)
        }
        for (i in 0 until genderList.length()) {
            var obj = genderList.getJSONObject(i)
            var cdata = CountryCodeData(
                Commonutils.hasStringForKey(obj, "id"),
                Commonutils.hasStringForKey(obj, "name")
            )
            Constants.genderlist.add(cdata)
        }
        for (i in 0 until rangeList.length()) {
            var obj = rangeList.getJSONObject(i)
            var cdata = RangeData(
                Commonutils.hasStringForKey(obj, "id"),
                Commonutils.hasStringForKey(obj, "value"),
                Commonutils.hasStringForKey(obj, "temperature"),
                Commonutils.hasStringForKey(obj, "celsius")
            )
            Constants.rangelist.add(cdata)
        }





        dismissProgressBar()
        if(Commonutils.getString(pref!!,Constants.Authorize)!!.length>0) {
            startActivity(Intent(this, MainActivity::class.java))
        }else{
            startActivity(Intent(this, LoginActivity::class.java))
        }

        finish()
    }
}