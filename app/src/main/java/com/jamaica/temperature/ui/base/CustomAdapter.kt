package com.jamaica.temperature.ui.base


import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.amberpaymerchant.ui.base.RecyclerViewCallBack
import com.jamaica.temperature.R
import com.jamaica.temperature.extensions.inflate
import com.jamaica.temperature.ui.data.HistoryData
import com.jamaica.temperature.utils.Commonutils
import com.jamaica.temperature.utils.Constants
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CustomAdapter(var c:Context,var list:ArrayList<HistoryData>,_callback:Any) : RecyclerView.Adapter<CustomAdapter.ItemViewHolder>() {

    lateinit var pref: SharedPreferences
    private val TYPE_HEADER = 0
    private val TYPE_ITEM = 1

    private var viewitemlists: ArrayList<HistoryData>? = null
    private var context: Context? = null
    var _callback:Any? = null

    init {
        context = c
        viewitemlists = list
        this._callback = _callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
pref = context!!.getSharedPreferences(Constants.PrefName, Context.MODE_PRIVATE)
        val inflatedView = parent.inflate(R.layout.row_historylist, false)
            return ItemViewHolder(inflatedView,_callback!!)
    }

    override fun getItemCount(): Int {
        return viewitemlists!!.size
    }


    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
holder.bindData(viewitemlists!![position])
    }

  /*  inner class HeaderViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
//        var txt_header: TextView
//        var txt_planned: TextView
//        var txt_inprogress: TextView
//        var txt_completed: TextView

        init {
//            txt_header =
//                view.findViewById<View>(R.id.txt_header) as TextView
            *//*txt_planned =
                view.findViewById<View>(R.id.planned_requests_count_text) as TextView
            txt_inprogress =
                view.findViewById<View>(R.id.inprogress_count_text) as TextView
            txt_completed =
                view.findViewById<View>(R.id.completed_count_text) as TextView*//*
        }
    }
*/
    inner class ItemViewHolder(itemView: View,_callBack: Any) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
      private var callBack: Any = _callBack
//      private var dataBinding: Any = DataBindingUtil.bind(itemView)!!
      var title: TextView
      var tempvalue: TextView
      var symptom: LinearLayout
      var comment: TextView
      var status: ImageView
      var img1:ImageView
      var img2:ImageView
      var img3:ImageView
      var img4:ImageView
      var img5:ImageView
      var img6:ImageView
      var editlinear:LinearLayout
      var rowroot:LinearLayout
      var editImg : ImageView
      var deleteImg : ImageView
init {




    title = itemView.findViewById<View>(R.id.header_history_list) as TextView
    tempvalue =
        itemView.findViewById<View>(R.id.tempTxt) as TextView
    symptom =
        itemView.findViewById<View>(R.id.symtomLinear) as LinearLayout
    comment =
        itemView.findViewById<View>(R.id.commentTxt) as TextView
    status =
        itemView.findViewById<View>(R.id.statusImg) as ImageView
    img1 =
        itemView.findViewById(R.id.img1)
    img2 =
        itemView.findViewById(R.id.img2)
    img3 =
        itemView.findViewById(R.id.img3)
    img4 =
        itemView.findViewById(R.id.img4)
    img5 =
        itemView.findViewById(R.id.img5)
    img6 =
        itemView.findViewById(R.id.img6)

    editlinear =
        itemView.findViewById(R.id.editLinear)
    rowroot =
        itemView.findViewById(R.id.row_root)
    editImg =
        itemView.findViewById(R.id.editImg)
    deleteImg =
        itemView.findViewById(R.id.deleteImg)

//    itemView.setOnClickListener(this)
}

      fun bindData(data: Any) {
          var d = data as HistoryData
          Log.d("tag","adapter bind data on :26:"+d.addedday+"\n"+d.addedtime+"\n"+d.tempcels)



          if(d.addedday.length>0) {
              title.setText(d.addedday)
              title.visibility = View.VISIBLE
          }else{
              title.visibility = View.GONE
          }

          if(Commonutils.getBoolean(pref!!,Constants.CELSIUS_OR_TEMP)!!){
              tempvalue.setText(d.tempcels +" "+d.templbl)
          }else{
              tempvalue.setText(d.tempvalue+" "+d.templbl)
          }

          tempvalue.setTextColor(d.color)

          status.setImageResource(d.img)

          comment.setText(d.descr)

          var syms : List<String>? = null

//          var liner = LinearLayout(context)
img1.visibility = View.GONE
img2.visibility = View.GONE
img3.visibility = View.GONE
img4.visibility = View.GONE
img5.visibility = View.GONE
img6.visibility = View.GONE

          if(d.symptom.contains(",")){
              syms = d.symptom.split(",")
            /*
              var p = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT)
              liner.orientation = LinearLayout.HORIZONTAL
              liner.layoutParams = p
*/
              for (i in 0 until syms.size){
                  when(syms[i].toInt()){
                      1 -> {
                          img1.visibility = View.VISIBLE
                      }
                      2 -> {

                          img2.visibility = View.VISIBLE
                      }
                      3 -> {
                          img3.visibility = View.VISIBLE

                      }
                      4 -> {
                          img4.visibility = View.VISIBLE

                      }
                      5 -> {
                          img5.visibility = View.VISIBLE

                      }
                      6 ->{
                          img6.visibility = View.VISIBLE

                      }
                  }
              }
          }else {

              when (d.symptom.toInt()) {
                  1 -> {
                      img1.visibility = View.VISIBLE
                  }
                  2 -> {

                      img2.visibility = View.VISIBLE
                  }
                  3 -> {
                      img3.visibility = View.VISIBLE

                  }
                  4 -> {
                      img4.visibility = View.VISIBLE

                  }
                  5 -> {
                      img5.visibility = View.VISIBLE

                  }
                  6 -> {
                      img6.visibility = View.VISIBLE

                  }
              }
          }

          rowroot.setOnClickListener({
              val value = Date()
              val dateFormatter = SimpleDateFormat("dd MMM yy")
              var ct = dateFormatter.format(value)
              Log.d("tag","current date :"+ct+"\n "+d.addedday)
              if(ct.equals(d.addedday)) {
                  editlinear.visibility = View.VISIBLE
              }
          })

          editlinear.setOnClickListener({
              editlinear.visibility = View.GONE
          })

          editImg.setOnClickListener({
              editlinear.visibility == View.GONE
              editlinear.visibility == View.GONE

              if (callBack is RecyclerViewCallBack) {
                  (callBack as RecyclerViewCallBack).onBindData(
                      "",
                      data,
                      1
                  )
              } else if (callBack is RecyclerViewCallBackNew) {
                  (callBack as RecyclerViewCallBackNew).onBindData(
                      "",
                      data,
                      1
                  )
              }
          })

          deleteImg.setOnClickListener({
              editlinear.visibility == View.GONE
              editlinear.visibility == View.GONE
              if (callBack is RecyclerViewCallBack) {
                  (callBack as RecyclerViewCallBack).onBindData(
                      "",
                      data,
                      2
                  )
              } else if (callBack is RecyclerViewCallBackNew) {
                  (callBack as RecyclerViewCallBackNew).onBindData(
                      "",
                      data,
                      2
                  )
              }
          })

//          symptom.addView(liner)



        /*  dataBinding.apply {
              if (callBack is RecyclerViewCallBack) {
                  (callBack as RecyclerViewCallBack).onBindData(
                      dataBinding,
                      data,
                      adapterPosition
                  )
              } else if (callBack is RecyclerViewCallBackNew) {
                  (callBack as RecyclerViewCallBackNew).onBindData(
                      dataBinding,
                      data,
                      adapterPosition
                  )
              }

              setItemClick(data)
          }*/
      }

/*
      private fun setItemClick(model: Any) {
          itemView.setOnClickListener {
              if (callBack is RecyclerViewCallBack) {
                  (callBack as RecyclerViewCallBack).onItemClick(model, adapterPosition)
              } else {
                  (callBack as RecyclerViewCallBackNew).onItemClick(
                      itemView,
                      model,
                      adapterPosition
                  )
              }

          }
      }

    */  //3


      override fun onClick(p0: View?) {

      }
  }
}