package com.amberpaymerchant.ui.base

import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.jamaica.temperature.extensions.inflate
import com.jamaica.temperature.ui.base.RecyclerViewCallBackNew
import com.jamaica.temperature.ui.data.HealthHistoryData
import java.util.ArrayList


open class RecyclerViewAdapter(
    private var layoutId: Int,
    var callBack: Any,
    var items : ArrayList<HealthHistoryData>
) : RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {

//    private val items = mutableListOf<Any>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val viewDataBinding = parent.inflate(layoutId)
        return ItemViewHolder(viewDataBinding)
    }

    /**
     *  Function to bind each item in the items list to the RecyclerView at a given position
     */
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindData(items[position])
    }

    /**
     *  Function to get the total number of items in the RecyclerView
     */
    override fun getItemCount(): Int = items.size

    /**
     *  Function to add single item to the RecyclerView
     */
    fun addItem(item: HealthHistoryData) {
        this.items.add(item)
        notifyItemInserted(items.size - 1)
    }

    /**
     *  Function to add a list of items to the RecyclerView
     */
    fun addItems(items: List<HealthHistoryData>) = this.items.addAll(items)

    fun updateItem(item: HealthHistoryData) {
        val position = items.indexOf(item)
        items[position] = item
        notifyItemChanged(position)
    }

    /**
     *  Function to clear the RecyclerView
     */
    fun clearItems() {
        this.items.clear()
        notifyDataSetChanged()
    }

    /**
     *  Function to clear the RecyclerView
     */
    fun clearItem(position: Int) {
        try {
            items.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, items.size)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     *  Function to clear the RecyclerView
     */
    fun clearItem(item: Any) {
        try {
            val position = items.indexOf(item)
            if (items.size > position) {
                items.removeAt(position)
            }
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, items.size)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getItem(position: Int): Any = items.get(position)
    fun getList(): Any = items

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }
    /**
     *  The ViewHolder class which binds the data to the RecyclerView item
     */
    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var dataBinding: Any = DataBindingUtil.bind(itemView)!!


        fun bindData(data: Any) {
            dataBinding.apply {
                if (callBack is RecyclerViewCallBack) {
                    (callBack as RecyclerViewCallBack).onBindData(
                        dataBinding,
                        data,
                        adapterPosition
                    )
                } else if (callBack is RecyclerViewCallBackNew) {
                    (callBack as RecyclerViewCallBackNew).onBindData(
                        dataBinding,
                        data,
                        adapterPosition
                    )
                }

                setItemClick(data)
            }
        }

        private fun setItemClick(model: Any) {
            itemView.setOnClickListener {
                if (callBack is RecyclerViewCallBack) {
                    (callBack as RecyclerViewCallBack).onItemClick(model, adapterPosition)
                } else {
                    (callBack as RecyclerViewCallBackNew).onItemClick(
                        itemView,
                        model,
                        adapterPosition
                    )
                }

            }
        }


    }
}