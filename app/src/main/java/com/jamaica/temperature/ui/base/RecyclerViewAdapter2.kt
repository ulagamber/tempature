package com.jamaica.temperature.ui.base

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.amberpaymerchant.ui.base.RecyclerViewCallBack
import com.jamaica.temperature.R
import com.jamaica.temperature.databinding.LayoutChildHealthHistoryBinding
import com.jamaica.temperature.extensions.inflate
import com.jamaica.temperature.ui.data.HealthHistoryData

class RecyclerViewAdapter2(var layout:Int,var callBack:Any,private val photos: ArrayList<HealthHistoryData>) : RecyclerView.Adapter<RecyclerViewAdapter2.PhotoHolder>(){

    class PhotoHolder(v: View,_callBack:Any) : RecyclerView.ViewHolder(v), View.OnClickListener {


        //2
//        private var view: View = v
//        private var photo: HealthHistoryData? = null
        private var callBack: Any = _callBack
        private var dataBinding: Any = DataBindingUtil.bind(itemView)!!


        fun bindData(data: Any) {
            var d = data as HealthHistoryData
            Log.d("tag","adapter bind data on :26:"+d.addedday+"\n"+d.addedtime+"\n"+d.tempcels)
            dataBinding.apply {
                if (callBack is RecyclerViewCallBack) {
                    (callBack as RecyclerViewCallBack).onBindData(
                        dataBinding,
                        data,
                        adapterPosition
                    )
                } else if (callBack is RecyclerViewCallBackNew) {
                    (callBack as RecyclerViewCallBackNew).onBindData(
                        dataBinding,
                        data,
                        adapterPosition
                    )
                }

                setItemClick(dataBinding,data)
            }
        }


        private fun setItemClick(dataBinding: Any,model: Any) {
            itemView.setOnClickListener {
                if (callBack is RecyclerViewCallBack) {
                    (callBack as RecyclerViewCallBack).onItemClick(model, adapterPosition)
                } else {
                    (callBack as RecyclerViewCallBackNew).onItemClick(
                        itemView,
                        model,
                        adapterPosition
                    )
                }

            }
        }

        //3
        init {
            v.setOnClickListener(this)
        }

        //4
        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")
        }

        companion object {
            //5
            private val PHOTO_KEY = "PHOTO"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoHolder {
        val inflatedView = parent.inflate(R.layout.layout_child_health_history, false)
        Log.d("tag","adapter bind data on  81:")
        return PhotoHolder(inflatedView,callBack)
    }

    override fun getItemCount(): Int {
        Log.d("tag","adapter bind data on  85 :"+photos.size)
        return photos.size
    }

    override fun onBindViewHolder(holder: PhotoHolder, position: Int) {
        Log.d("tag","adapter bind data on  90 position:"+position)
holder.bindData(photos[position])
    }

}