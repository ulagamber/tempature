package com.jamaica.temperature.ui.base

import android.view.View

interface RecyclerViewCallBackNew {

    fun onBindData(dataBinding: Any, model: Any, position: Int)

    fun onItemClick(view :View, model: Any, position: Int)
}