package com.jamaica.temperature.ui.data

class CountryCodeData {
    var id : String? = ""
    var code : String? = ""
    var name : String? = ""
    constructor(_id : String?,_code : String?,_name : String?){
        this.id = _id;
        this.code = _code
        this.name = _name
    }
    constructor(_id : String?,_name : String?){
        this.id = _id;
        this.name = _name
    }
}