package com.jamaica.temperature.ui.data

data class HistoryData(var id : String,var symptom : String,var descr : String,var tempvalue : String,
                       var tempcels : String,var templbl : String,var checktime : String,
                       var addedday : String,var addedtime : String,var type:String,var color:Int,var img:Int) {

}