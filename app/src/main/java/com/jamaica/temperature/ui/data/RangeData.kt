package com.jamaica.temperature.ui.data

class RangeData {
    var id : String? = ""
    var value : String? = ""
    var temperature : String? = ""
    var celsius : String? = ""
    constructor(_id : String?,_value : String?,_temperature : String?,_celsius:String?){
        this.id = _id;
        this.value = _value
        this.temperature = _temperature
        this.celsius = _celsius
    }

}