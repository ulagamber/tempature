package com.jamaica.temperature.ui.data

data class UserData(var _id:String,var _name:String,var _lastname:String,
                    var _address:String,var _gender:String,var _dob:String,
                    var _image:String,
                    var _dialing:String,
                    var _mobile:String,
                    var _email:String,var _username:String,var _paitentId:String,var _status:String,var _usertype:String,
                    var _staffId:String){

}
/*
class UserDetailsData {
    var id:String = ""
    var name:String = ""
    var lastname:String = ""
    var address:String = ""
    var gender:String = ""
    var dob:String = ""
    var profileimage:String = ""
    var dialingCode:String = ""
    var mobile:String = ""
    var email:String = ""
    var username:String = ""
    var paitentId:String = ""
    var status:String = ""
    var usertype:String = ""
    var staffId:String = ""

    constructor(_id:String,_name:String,_lastname:String,_address:String,_gender:String,_dob:String,_image:String,
    _dialing:String,_mobile:String,_email:String,_username:String,_paitentId:String,_status:String,_usertype:String,_staffId:String)
}*/
