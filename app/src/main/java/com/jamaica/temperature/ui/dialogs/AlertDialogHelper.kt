package com.jamaica.temperature.ui.dialogs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog

import com.google.android.material.textview.MaterialTextView
import com.jamaica.temperature.R

@SuppressLint("InflateParams")
class AlertDialogHelper(
    context: Context,
    title: CharSequence?,
    message: CharSequence?,
    isMultiple: Boolean = true
) {

    private var dialog: AlertDialog? = null
    var cancelable: Boolean = true

    private val dialogView: View by lazy {
        LayoutInflater.from(context).inflate(R.layout.dialog_info, null)
    }

    private val builder: AlertDialog.Builder = AlertDialog.Builder(context).setView(dialogView)

    private val title: TextView by lazy {
        dialogView.findViewById<MaterialTextView>(R.id.tvTitle)
    }

    private val message: TextView by lazy {
        dialogView.findViewById<MaterialTextView>(R.id.tvMessage)
    }

    private val positiveButton: Button by lazy {
        dialogView.findViewById<Button>(R.id.btnOK)
    }

    private val negativeButton: Button by lazy {
        dialogView.findViewById<Button>(R.id.btnCancel)
    }

    init {
        this.title.text = title
        this.message.text = message
        if (isMultiple) {
            negativeButton.visibility = View.VISIBLE
        } else {
            negativeButton.visibility = View.GONE
        }
    }

    fun positiveButton(@StringRes textResource: Int, func: (() -> Unit)? = null) {
        with(positiveButton) {
            text = builder.context.getString(textResource)
            setClickListener(func)
        }
    }

    fun positiveButton(text: CharSequence, func: (() -> Unit)? = null) {
        with(positiveButton) {
            this.text = text
            setClickListener(func)
        }
    }

    fun negativeButton(@StringRes textResource: Int, func: (() -> Unit)? = null) {
        with(negativeButton) {
            text = builder.context.getString(textResource)
            setClickListener(func)
        }
    }

    fun negativeButton(text: CharSequence, func: (() -> Unit)? = null) {
        with(negativeButton) {
            this.text = text
            setClickListener(func)
        }
    }

    fun onCancel(func: () -> Unit) {
        builder.setOnCancelListener { func() }
    }

    fun create(): AlertDialog {
        title.goneIfTextEmpty()
        message.goneIfTextEmpty()
        positiveButton.goneIfTextEmpty()
        negativeButton.goneIfTextEmpty()

        dialog = builder
            .setCancelable(cancelable)
            .create()
        return dialog!!
    }

    private fun TextView.goneIfTextEmpty() {
        visibility = if (text.isNullOrEmpty()) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

    private fun Button.setClickListener(func: (() -> Unit)?) {
        setOnClickListener {
            func?.invoke()
            dialog?.dismiss()
        }
    }
}