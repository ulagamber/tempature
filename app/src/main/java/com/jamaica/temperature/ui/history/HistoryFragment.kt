package com.jamaica.temperature.ui.history

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.amberpaymerchant.ui.base.RecyclerViewCallBack
import com.jamaica.temperature.R
import com.jamaica.temperature.extensions.showErrorMessage
import com.jamaica.temperature.service.AsyncTaskCompleteListener
import com.jamaica.temperature.service.DataBindingService
import com.jamaica.temperature.service.GetJsonParams
import com.jamaica.temperature.service.PostDataHelperWithAsyncHttpClient
import com.jamaica.temperature.ui.PaginationListener
import com.jamaica.temperature.ui.base.CustomAdapter
import com.jamaica.temperature.ui.data.HistoryData
import com.jamaica.temperature.utils.APIUtils
import com.jamaica.temperature.utils.Commonutils
import com.jamaica.temperature.utils.Constants
import com.jamaica.temperature.utils.NetworkUtils
import com.loopj.android.http.AsyncHttpResponseHandler
import com.valdesekamdem.library.mdtoast.MDToast
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.fragment_history.*
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class HistoryFragment : Fragment(),AsyncTaskCompleteListener<String>, RecyclerViewCallBack {

    private var root: View? = null
    private lateinit var historyAdapter: CustomAdapter
    private var pref: SharedPreferences?=null
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var historyViewModel: HistoryViewModel
var page:Int = 1

    var API_TYPE = 1
    val API_EDIT = 5
    val API_DELETE = 6
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        pref = activity?.getSharedPreferences(Constants.PrefName, Context.MODE_PRIVATE)
        historyViewModel =
            ViewModelProviders.of(this).get(HistoryViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_history
            , container, false)
//        val textView: TextView = root.findViewById(R.id.text_dashboard)
//        historyViewModel.text.observe(this, Observer {
//            textView.text = it
//        })

getHistoryList(page)

        return root
    }

    private fun getHistoryList(page: Int) {
        var token = Commonutils.getString(pref!!, Constants.Authorize)
        DataBindingService(
            activity,
            this,
            APIUtils.GET_HISTORY + "?page="+page,
            "",
            APIUtils.GET,token
        ).execute("")

    }

var isLastPage:Boolean = false
var isLoading:Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        history_Reclyer.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(activity)
            // set the custom adapter to the RecyclerView
//            adapter = RecyclerAdapter
        }

        linearLayoutManager = LinearLayoutManager(activity)
//showHealth_Reclyer.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true))

        history_Reclyer.layoutManager = linearLayoutManager



        history_Reclyer.addOnScrollListener(object : PaginationListener(linearLayoutManager) {
             override fun loadMoreItems() {
                isLoading = true
                page++
                 getHistoryList(page)
            }

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }
        })
//        history_Reclyer.addOnScrollListener(pagelistener)

    }

    override fun onTaskComplete(code: Int, result: String?) {
        if(code == 200){
            Log.d("tag","history list respose:"+result)
            var obj = JSONObject(result)


            var data = Commonutils.hasJsonForKey(obj, "data")
            var tPage = Commonutils.hasStringForKey(data,"total_pages")
            // check weather is last page or not
            var totalPage = tPage.toInt()
            if (page < totalPage) {

            } else {
                isLastPage = true
            }
            isLoading = false

            var arry = Commonutils.hasArrayForKey(data, "health_history")
            var l = arry.length()
if(page==1) {
    Constants.HistoryList.clear()
}
            var pre_day = ""
            for (i in 0 until l) {
                var obj1 = arry.getJSONObject(i)
                var health_id = Commonutils.hasStringForKey(obj1, "id")
                var symptom_id = Commonutils.hasStringForKey(obj1, "symptoms_id")
                var health_description = Commonutils.hasStringForKey(obj1, "health_description")
                var temperature_value = Commonutils.hasStringForKey(obj1, "temperature_value")
                var temperature_celsius =
                    Commonutils.hasStringForKey(obj1, "temperature_celsius")


                var temperature_lbl = Commonutils.hasStringForKey(obj1, "temperature_lbl")
                var checkup_time = Commonutils.hasStringForKey(obj1, "checkup_time")


                var added_day = Commonutils.hasStringForKey(obj1, "added_day")
                if(pre_day!=""){
                    if(pre_day.equals(added_day)){
                        added_day = ""
                    }else{
                        pre_day = added_day
                    }
                }else{
                    pre_day = added_day
                }

                var added_time = Commonutils.hasStringForKey(obj1, "added_time")
                var type = Commonutils.hasStringForKey(obj1, "type")

                var t = temperature_value.replace(" °F","")
                var c = temperature_celsius.replace(" °C","")

                var temps = t.toFloat()
                var cels = c.toFloat()

                var statusImg = R.drawable.ic_temperature_history_blue

                var l = Constants.rangelist.size

                var healthTempColor = activity?.resources?.getColor(R.color.low_temp)

                for (i in 0 until l){
                    var d = Constants.rangelist.get(i)
                    var Tempsplit = d.temperature?.split("-")
                    var Celsplit = d.celsius?.split("-")

                    var TempMin = Tempsplit?.get(0)?.toFloat()
                    var TempMax = Tempsplit?.get(1)?.toFloat()

                    var CelMin = Celsplit?.get(0)?.toFloat()
                    var CelMax = Celsplit?.get(1)?.toFloat()


//            var c =cels.coerceIn(CelMin,CelMax)

                    when(d.id?.toInt()){
                        1 -> {
                            Log.d ("Tag","cels 1:"+cels)
                            if(cels >= CelMin!!){
                                if(cels <= CelMax!!) {
                                    healthTempColor = activity?.resources?.getColor(R.color.low_temp)
                                    statusImg = R.drawable.ic_temperature_history_blue
                                }
                            }

                            if(temps>=TempMin!!) {
                                if(temps <= TempMax!!) {
                                    healthTempColor = activity?.resources?.getColor(R.color.low_temp)
                                    statusImg = R.drawable.ic_temperature_history_blue
                                }
                            }
                        }
                        2 -> {
                            Log.d ("Tag","cels 2:"+cels)

                            if(cels >= CelMin!!){
                                if(cels <= CelMax!!){
                                    healthTempColor = activity?.resources?.getColor(R.color.normal_temp)
                                    statusImg = R.drawable.ic_temperature_history_green
                                }
                            }

                            if(temps>=TempMin!!) {
                                if(temps <= TempMax!!) {
                                    healthTempColor = activity?.resources?.getColor(R.color.normal_temp)
                                    statusImg = R.drawable.ic_temperature_history_green
                                }
                            }
                        }
                        3 -> {
                            Log.d ("Tag","cels3:"+cels)

                            if(cels >= CelMin!!){
                                if(cels <= CelMax!!){
                                    healthTempColor = activity?.resources?.getColor(R.color.fever_temp)
                                    statusImg = R.drawable.ic_temperature_history_orange
                                }
                            }

                            if(temps>=TempMin!!) {
                                if(temps <= TempMax!!) {
                                    healthTempColor = activity?.resources?.getColor(R.color.fever_temp)
                                    statusImg = R.drawable.ic_temperature_history_orange
                                }
                            }
                        }
                        4 -> {
                            Log.d ("Tag","cels4:"+cels)

                            if(cels >= CelMin!!){
//                            if(cels <= CelMax!!){
                                healthTempColor = activity?.resources?.getColor(R.color.high_fever_temp)
                                statusImg = R.drawable.ic_temperature_history_red
//                            }
                            }

                            if(temps>=TempMin!!) {

                                healthTempColor = activity?.resources?.getColor(R.color.high_fever_temp)
                                statusImg = R.drawable.ic_temperature_history_red

                            }
                        }
                    }



                }

                var hData = HistoryData(
                    health_id,
                    symptom_id,
                    health_description,
                    temperature_value,
                    temperature_celsius,
                    temperature_lbl,
                    checkup_time,
                    added_day,
                    added_time,
                    type,
                    healthTempColor!!,
                    statusImg
                )
                Constants.HistoryList.add(hData)
            }

            historyAdapter = CustomAdapter(activity!!.applicationContext,Constants.HistoryList,this@HistoryFragment)
            history_Reclyer.adapter = historyAdapter
        }
    }

    fun headerDate(dateString:String):String{
        val formatter = SimpleDateFormat("dd MMM yy")
//        formatter.timeZone = TimeZone.getTimeZone(servertz)
        var value: Date? = null
        try {
            value = formatter.parse(dateString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val dateFormatter = SimpleDateFormat("dd MMM")
//        dateFormatter.timeZone = TimeZone.getDefault()

        return dateFormatter.format(value)
    }

    override fun onBindData(dataBinding: Any, model: Any, position: Int) {

            /* latest_feed_layout =
               itemView.findViewById<View>(R.id.latest_feed_layout) as LinearLayout
           upvote_image = itemView.findViewById<View>(R.id.upward_image) as LikeButton*/
        model as HistoryData
if(position == 1){
    updateDialog(model.id,model.tempvalue,model.tempcels,model.descr,model.symptom)
}

        if(position ==2){
            deleteTodayTemp(model.id)
        }
    }

    override fun onItemClick(model: Any, position: Int) {

    }


    private fun deleteTodayTemp(id: String) {

        val keys = ArrayList<String>()
        val values = ArrayList<String>()

        keys.add("id")
        values.add(""+id)
        val params = GetJsonParams()
        var p: String? = null
        try {
            p = params.conver_Json(keys, values)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if(NetworkUtils.isNetWorkAvailable(activity!!)){

            var token = Commonutils.getString(pref!!,Constants.Authorize)
            API_TYPE = API_DELETE
            deleteTemperatureServ(keys,values)
        }else{
            showErrorMessage(activity!!,activity!!.getString(R.string.api_no_internet))
        }
    }

    private fun deleteTemperatureServ(keys: ArrayList<String>, values: ArrayList<String>) {
        var token = Commonutils.getString(pref!!,Constants.Authorize)
        val client = PostDataHelperWithAsyncHttpClient()
        client.post(
            keys,
            values,
            activity!!,
            APIUtils.DELETE_HISTORY,""+token,
            object : AsyncHttpResponseHandler() {


                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray
                ) {
                    run {
                        val response = String(responseBody)

                        onTaskComplete(200, response)

                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray,
                    error: Throwable
                ) {

                }
            })
    }

    private fun editTodayTemp(id: String, tempvalue: String, tempcels: String,des: String,symtom:String) {
        SymtomId  = java.lang.StringBuilder()
        updateDialog(id,tempvalue,tempcels,des,symtom)
    }

    lateinit var symfiveImg : ImageView
    lateinit var symfourImg : ImageView
    lateinit var symthreeImg : ImageView
    lateinit var symtwoImg : ImageView
    lateinit var symoneImg : ImageView
    lateinit var symsixImg : ImageView
    var SymtomId : StringBuilder = java.lang.StringBuilder()

    private fun isInRange(a: Int, b: Int, c: Int): Boolean {
        return if (b > a) c >= a && c <= b else c >= b && c <= a
    }

    fun updateDialog(id: String,temp:String,tempcels:String,des:String,symtom:String) {

        Log.d("tag","id:"+id+"\ntemp:"+temp+"\ncel:"+tempcels+"\ndes:"+des)

        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.layout_check_temp_dialog)
        val value = Date()
        val dateFormatter = SimpleDateFormat("hh-mm-aa")
        val dateFormatterAPI = SimpleDateFormat("hh:mm:ss")

//        dateFormatter.timeZone = TimeZone.getDefault()
//        dateFormatterAPI.timeZone = TimeZone.getDefault()

        var ct = dateFormatter.format(value)
        var ctAPI = dateFormatterAPI.format(value)

        var split = ct.split("-")
        var l = split.size


        val msgTxt = dialog.findViewById<View>(R.id.currentTimeTxt) as TextView
        val timeunitTxt = dialog.findViewById<View>(R.id.timeUnitTxt) as TextView
        val tempunitTxt = dialog.findViewById<View>(R.id.tempUnitTxt) as TextView
        val descTxt = dialog.findViewById<View>(R.id.commentsTxt) as EditText
        msgTxt.setText(split[0]+" "+split[1])
        timeunitTxt.setText(split[2])

//        val numOne = dialog.findViewById<View>(R.id.numberOnepick) as NumberPicker
//        val numTwo = dialog.findViewById<View>(R.id.numberTwopick) as NumberPicker

        val numOne = dialog.findViewById<View>(R.id.tempOneEdt) as EditText
        val numTwo = dialog.findViewById<View>(R.id.tempTwoEdt) as EditText
        numOne.requestFocus()

        val celMx = Commonutils.getString(pref!!,Constants.CELSIUS_END)
        val celMn = Commonutils.getString(pref!!,Constants.CELSIUS_START)
        val TemMn = Commonutils.getString(pref!!,Constants.TEMP_START)
        val TemMx = Commonutils.getString(pref!!,Constants.TEMP_END)


        val values =
            arrayOf("00", "10", "20", "30", "40", "50", "60", "70", "80", "90")

//        numTwo.displayedValues = values
//        numTwo.maxValue = 9
//        numTwo.minValue = 0
        var c = tempcels.replace(" °C","")
        var t = temp.replace(" °F","")
        var cels = c.toFloat()
        var temps = t.toFloat()
        var Min_One_edt = 0
        var Max_One_edt = 0

        if(Commonutils.getBoolean(pref!!,Constants.CELSIUS_OR_TEMP)!!) {
            Min_One_edt = celMn!!.toInt()
            Max_One_edt = celMx!!.toInt()

            var sp = ""+cels
            if(sp.contains(".")) {
                var spt = sp.split(".")
                numOne.setText(spt[0],TextView.BufferType.EDITABLE)
                numTwo.setText(spt[1],TextView.BufferType.EDITABLE)
            }else{
                numOne.setText(cels.toString(),TextView.BufferType.EDITABLE)
                numTwo.setText("0")
            }
            tempunitTxt.setText(" °C")
        }else{
            Min_One_edt = TemMn!!.toInt()
            Max_One_edt = TemMx!!.toInt()

            var sp = ""+temps
            if(sp.contains(".")) {
                var spt = sp.split(".")
                numOne.setText(spt[0],TextView.BufferType.EDITABLE)
                numTwo.setText(spt[1],TextView.BufferType.EDITABLE)
            }else{
                numOne.setText(temps.toString(),TextView.BufferType.EDITABLE)
                numTwo.setText("0")
            }
            tempunitTxt.setText(" °F")
        }
        SymtomId.clear()
        SymtomId.append(symtom)

        var symone = dialog.findViewById<View>(R.id.symoneTxt) as TextView
        symoneImg = dialog.findViewById<View>(R.id.check_temp_symone) as ImageView
        var symtwo = dialog.findViewById<View>(R.id.symtwoTxt) as TextView
        symtwoImg = dialog.findViewById<View>(R.id.check_temp_symtwo) as ImageView
        var symthree = dialog.findViewById<View>(R.id.symthreeTxt) as TextView
        symthreeImg = dialog.findViewById<View>(R.id.check_temp_symthree) as ImageView
        var symfour = dialog.findViewById<View>(R.id.symfourTxt) as TextView
        symfourImg = dialog.findViewById<View>(R.id.check_temp_symfour) as ImageView
        var symfive = dialog.findViewById<View>(R.id.symfiveTxt) as TextView
        symfiveImg = dialog.findViewById<View>(R.id.check_temp_symfive) as ImageView
        var symsix = dialog.findViewById<View>(R.id.symsixTxt) as TextView
        symsixImg = dialog.findViewById<View>(R.id.check_temp_symsix) as ImageView


        var cancel = dialog.findViewById<View>(R.id.btn_check_cancel) as Button
        var submit = dialog.findViewById<View>(R.id.btn_check_sumbit) as Button

        cancel.setOnClickListener({
            dialog.dismiss()
        })
        descTxt.setText(""+des)
        submit.setOnClickListener({

            var vv = numOne.text.toString()
            Log.d("tag","val is with range:"+isInRange(Min_One_edt, Max_One_edt, vv.toInt()))
            var isRange = isInRange(Min_One_edt, Max_One_edt, vv.toInt())
            if(!isRange){
                MDToast.makeText(context,"Temperature value should be minimum $Min_One_edt and maximum $Max_One_edt")
                return@setOnClickListener
            }

            var time = ctAPI
            var temp_type = 1
            if(Commonutils.getBoolean(pref!!,Constants.CELSIUS_OR_TEMP)!!){
                temp_type = 2
            }
            var value = numOne.text.toString()+"."+numTwo.text.toString()
            var symtom = SymtomId
            var desc = descTxt.text.toString().trim()


            val keys = ArrayList<String>()
            val values = ArrayList<String>()

            keys.add("id")
            values.add(""+id)
            keys.add("checkup_time")
            values.add(""+time)
            keys.add("temerature_type")
            values.add(""+temp_type)
            keys.add("temperature")
            values.add(""+value)
            keys.add("symptoms_id")
            values.add(""+symtom)
            keys.add("health_description")
            values.add(""+desc)
            val params = GetJsonParams()
            var p: String? = null
            try {
                p = params.conver_Json(keys, values)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            if(NetworkUtils.isNetWorkAvailable(activity!!)){

                var token = Commonutils.getString(pref!!,Constants.Authorize)
                API_TYPE = API_EDIT
                /* DataBindingService(
                     activity,
                     this,
                     APIUtils.ADD_TEMPERATURE,
                     ""+p,
                     APIUtils.POST,token
                 ).execute("")
 */
                editTemperatureServ(keys,values)
                dialog.dismiss()
            }else{
                showErrorMessage(activity!!,activity!!.getString(R.string.api_no_internet))
            }
        })

        symone.setText(Constants.symptomlist.get(0).name)
        symtwo.setText(Constants.symptomlist.get(1).name)
        symthree.setText(Constants.symptomlist.get(2).name)
        symfour.setText(Constants.symptomlist.get(3).name)
        symfive.setText(Constants.symptomlist.get(4).name)
        symsix.setText(Constants.symptomlist.get(5).name)

        symoneImg.setOnClickListener({
            symtomsFun(it.id)
        })
        symtwoImg.setOnClickListener({
            symtomsFun(it.id)
        })
        symthreeImg.setOnClickListener({
            symtomsFun(it.id)
        })
        symfourImg.setOnClickListener({
            symtomsFun(it.id)
        })
        symfiveImg.setOnClickListener({
            symtomsFun(it.id)
        })
        symsixImg.setOnClickListener({
            symtomsFun(it.id)
        })




        //val no_thanks = dialog.findViewById<View>(R.id.call_back_txt) as TextView
//        numOne.setFormatter(NumberPicker.Formatter { i -> String.format("%02d", i) })
//        numTwo.setFormatter(NumberPicker.Formatter { i -> String.format("%02d", i) })
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
    }

    private fun editTemperatureServ(keys: ArrayList<String>, values: ArrayList<String>) {
        var token = Commonutils.getString(pref!!,Constants.Authorize)
        val client = PostDataHelperWithAsyncHttpClient()
        client.post(
            keys,
            values,
            activity!!,
            APIUtils.UPDATE_HISTORY,""+token,
            object : AsyncHttpResponseHandler() {


                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray
                ) {
                    run {
                        val response = String(responseBody)

                        onTaskComplete(200, response)

                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray,
                    error: Throwable
                ) {

                }
            })
    }

    private fun addTemperatureServ(keys: ArrayList<String>, values: ArrayList<String>) {
        var token = Commonutils.getString(pref!!,Constants.Authorize)
        val client = PostDataHelperWithAsyncHttpClient()
        client.post(
            keys,
            values,
            activity!!,
            APIUtils.ADD_TEMPERATURE,""+token,
            object : AsyncHttpResponseHandler() {


                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray
                ) {
                    run {
                        val response = String(responseBody)

                        onTaskComplete(200, response)

                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray,
                    error: Throwable
                ) {

                }
            })
    }

    fun symtomsFun(id:Int){

        symfiveImg.alpha    = 1f
        symfourImg.alpha    = 1f
        symthreeImg.alpha   = 1f
        symtwoImg.alpha     = 1f
        symoneImg.alpha     = 1f

        when(id){
            R.id.check_temp_symone->{
                symfiveImg.alpha = 0.5f
                symfourImg.alpha = 0.5f
                symthreeImg.alpha = 0.5f
                symtwoImg.alpha = 0.5f
                symsixImg.alpha = 0.5f

                addSymtoms(Constants.symptomlist.get(0).id)

            }
            R.id.check_temp_symfive -> {
                addSymtoms(Constants.symptomlist.get(4).id)
            }
            R.id.check_temp_symfour -> {
                addSymtoms(Constants.symptomlist.get(3).id)
            }
            R.id.check_temp_symsix -> {
                addSymtoms(Constants.symptomlist.get(5).id)
            }
            R.id.check_temp_symthree -> {
                addSymtoms(Constants.symptomlist.get(2).id)
            }
            R.id.check_temp_symtwo -> {
                addSymtoms(Constants.symptomlist.get(1).id)
            }
        }
    }

    private fun addSymtoms(str: String?) {
//        val chars: CharArray = str!!.toCharArray()

        var l = SymtomId.length
        var repeated: Boolean = false

        for (j in 0 until l){
            if(SymtomId[j].toString() == str){
                repeated = true
                break
            }
        }

        if(!repeated){

            if(l>0) {
                SymtomId.append(","+str)
            }else{
                SymtomId.append(str)
            }
            Log.d("tag","Symtoms id:"+SymtomId.length+" == "+SymtomId.toString()+" == "+str)
        }
    }

}