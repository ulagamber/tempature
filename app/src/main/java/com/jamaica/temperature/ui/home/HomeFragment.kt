package com.jamaica.temperature.ui.home

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.jamaica.temperature.BR
import com.jamaica.temperature.MainActivity
import com.jamaica.temperature.R
import com.jamaica.temperature.databinding.LayoutChildHealthHistoryBinding
import com.jamaica.temperature.extensions.showErrorMessage
import com.jamaica.temperature.service.AsyncTaskCompleteListener
import com.jamaica.temperature.service.DataBindingService
import com.jamaica.temperature.service.GetJsonParams
import com.jamaica.temperature.service.PostDataHelperWithAsyncHttpClient
import com.jamaica.temperature.ui.base.RecyclerViewAdapter2
import com.jamaica.temperature.ui.base.RecyclerViewCallBackNew
import com.jamaica.temperature.ui.data.HealthHistoryData
import com.jamaica.temperature.utils.APIUtils
import com.jamaica.temperature.utils.Commonutils
import com.jamaica.temperature.utils.Constants
import com.jamaica.temperature.utils.NetworkUtils
import com.loopj.android.http.AsyncHttpResponseHandler
import com.valdesekamdem.library.mdtoast.MDToast
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.fragment_home.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment : Fragment(), AsyncTaskCompleteListener<String>,RecyclerViewCallBackNew {

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var homeViewModel: HomeViewModel
    var main:MainActivity = MainActivity()
    private var pref: SharedPreferences? = null
    lateinit var root:View
    var API_TYPE = -1
    val API_DASHBOARD = 3
    val API_ADDTEMP = 4
    val API_EDIT = 5
    val API_DELETE = 6

    var healthhistoryAdpter : RecyclerViewAdapter2? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)

        root = inflater.inflate(R.layout.fragment_home, container, false)
        pref = activity?.getSharedPreferences(Constants.PrefName, Context.MODE_PRIVATE)

     //   setup(root,inflater)

        initobserver()
        var binding = DataBindingUtil.bind<ViewDataBinding>(root)
        binding?.setVariable(BR.homeviewModel,homeViewModel)
binding?.executePendingBindings()
        root.findViewById<SwitchCompat>(R.id.iv_switchFToC).setOnCheckedChangeListener({_,isChecked->
Commonutils.saveBoolean(pref!!,Constants.CELSIUS_OR_TEMP,isChecked)
            if(healthhistoryAdpter!=null){
            healthhistoryAdpter?.notifyDataSetChanged()
            }
        })

        root.findViewById<Button>(R.id.btn_submit).setOnClickListener({
             SymtomId  = java.lang.StringBuilder()
            addTempDialog("")
        })


        return root
    }

    override fun onResume() {
        super.onResume()
        if(healthhistoryAdpter!=null){
            healthhistoryAdpter?.notifyDataSetChanged()
            showHealth_Reclyer.adapter = healthhistoryAdpter
showHealth_Reclyer.scrollToPosition(Constants.healthHistoryList.size-1)
        }
    }

    lateinit var symfiveImg : ImageView
    lateinit var symfourImg : ImageView
    lateinit var symthreeImg : ImageView
    lateinit var symtwoImg : ImageView
    lateinit var symoneImg : ImageView
    lateinit var symsixImg : ImageView
    var SymtomId : StringBuilder = java.lang.StringBuilder()

    fun addTempDialog(msg: String) {
        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.layout_check_temp_dialog)
        val value = Date()
        val dateFormatter = SimpleDateFormat("hh-mm-aa")
        val dateFormatterAPI = SimpleDateFormat("hh:mm:ss")

//        dateFormatter.timeZone = TimeZone.getDefault()
//        dateFormatterAPI.timeZone = TimeZone.getDefault()

        var ct = dateFormatter.format(value)
        var ctAPI = dateFormatterAPI.format(value)

        var split = ct.split("-")
        var l = split.size


        val msgTxt = dialog.findViewById<View>(R.id.currentTimeTxt) as TextView
        val timeunitTxt = dialog.findViewById<View>(R.id.timeUnitTxt) as TextView
        val tempunitTxt = dialog.findViewById<View>(R.id.tempUnitTxt) as TextView
        val tempminmaxTxt = dialog.findViewById<View>(R.id.tempminmaxTxt) as TextView
        val descTxt = dialog.findViewById<View>(R.id.commentsTxt) as EditText
msgTxt.setText(split[0]+" "+split[1])
        timeunitTxt.setText(split[2])

//        val numOne = dialog.findViewById<View>(R.id.numberOnepick) as NumberPicker
//        val numTwo = dialog.findViewById<View>(R.id.numberTwopick) as NumberPicker
        val numOne = dialog.findViewById<View>(R.id.tempOneEdt) as EditText
        val numTwo = dialog.findViewById<View>(R.id.tempTwoEdt) as EditText
numOne.requestFocus()
        val celMx = Commonutils.getString(pref!!,Constants.CELSIUS_END)
        val celMn = Commonutils.getString(pref!!,Constants.CELSIUS_START)
        val TemMn = Commonutils.getString(pref!!,Constants.TEMP_START)
        val TemMx = Commonutils.getString(pref!!,Constants.TEMP_END)

        var Min_One_edt = 0
        var Max_One_edt = 0
        if(Commonutils.getBoolean(pref!!,Constants.CELSIUS_OR_TEMP)!!) {
//            numOne.minValue = celMn!!.toInt()
//            numOne.maxValue = celMx!!.toInt()
            Log.d("tag","input filter for edittext min and max cels:"+celMn+" & "+celMx)
//            numOne.setFilters(arrayOf(InputFilterMinMax(""+celMn!!.toInt(), ""+celMx!!.toInt())))
//            numTwo.setFilters(arrayOf(InputFilterMinMax("0", "9")))
Min_One_edt = celMn!!.toInt()
            Max_One_edt = celMx!!.toInt()
            tempunitTxt.setText(" °C")
        }else{
//            numOne.minValue = TemMn!!.toInt()
//            numOne.maxValue = TemMx!!.toInt()
            Min_One_edt = TemMn!!.toInt()
            Max_One_edt = TemMx!!.toInt()
            Log.d("tag","input filter for edittext min and max temp:"+TemMn+" & "+TemMx)
//            numOne.setFilters(arrayOf(InputFilterMinMax(""+TemMn!!.toInt(), ""+TemMx!!.toInt())))
//            numTwo.setFilters(arrayOf(InputFilterMinMax("0", "9")))

            tempunitTxt.setText(" °F")
        }
        numOne.setText(""+Min_One_edt,TextView.BufferType.EDITABLE)
        val values =
            arrayOf("00", "10", "20", "30", "40", "50", "60", "70", "80", "90")

//        numTwo.displayedValues = values
//        numTwo.maxValue = 9
//        numTwo.minValue = 0

        var symone = dialog.findViewById<View>(R.id.symoneTxt) as TextView
        symoneImg = dialog.findViewById<View>(R.id.check_temp_symone) as ImageView
        var symtwo = dialog.findViewById<View>(R.id.symtwoTxt) as TextView
        symtwoImg = dialog.findViewById<View>(R.id.check_temp_symtwo) as ImageView
        var symthree = dialog.findViewById<View>(R.id.symthreeTxt) as TextView
        symthreeImg = dialog.findViewById<View>(R.id.check_temp_symthree) as ImageView
        var symfour = dialog.findViewById<View>(R.id.symfourTxt) as TextView
        symfourImg = dialog.findViewById<View>(R.id.check_temp_symfour) as ImageView
        var symfive = dialog.findViewById<View>(R.id.symfiveTxt) as TextView
        symfiveImg = dialog.findViewById<View>(R.id.check_temp_symfive) as ImageView
        var symsix = dialog.findViewById<View>(R.id.symsixTxt) as TextView
        symsixImg = dialog.findViewById<View>(R.id.check_temp_symsix) as ImageView


        var cancel = dialog.findViewById<View>(R.id.btn_check_cancel) as Button
        var submit = dialog.findViewById<View>(R.id.btn_check_sumbit) as Button

        cancel.setOnClickListener({
            dialog.dismiss()
        })

        submit.setOnClickListener({
var vv = numOne.text.toString()
            Log.d("tag","val is with range:"+isInRange(Min_One_edt, Max_One_edt, vv.toInt()))
            var isRange = isInRange(Min_One_edt, Max_One_edt, vv.toInt())
if(!isRange){
MDToast.makeText(context,"Temperature value should be minimum $Min_One_edt and maximum $Max_One_edt")
    return@setOnClickListener
}

            var time = ctAPI
            var temp_type = 1
            if(Commonutils.getBoolean(pref!!,Constants.CELSIUS_OR_TEMP)!!){
                temp_type = 2
            }
            var value = numOne.text.toString()+"."+numTwo.text.toString()
            var symtom = SymtomId
            var desc = descTxt.text.toString().trim()


            val keys = ArrayList<String>()
            val values = ArrayList<String>()

            keys.add("checkup_time")
            values.add(""+time)
            keys.add("temerature_type")
            values.add(""+temp_type)
            keys.add("temperature")
            values.add(""+value)
            keys.add("symptoms_id")
            values.add(""+symtom)
            keys.add("health_description")
            values.add(""+desc)
            val params = GetJsonParams()
            var p: String? = null
            try {
                p = params.conver_Json(keys, values)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            Log.d("tag","params:"+p)
            if(NetworkUtils.isNetWorkAvailable(activity!!)){

                /*var token = Commonutils.getString(pref!!,Constants.Authorize)
API_TYPE = API_ADDTEMP
                DataBindingService(
                    activity,
                    this,
                    APIUtils.ADD_TEMPERATURE,
                    ""+p,
                    APIUtils.POST,token
                ).execute("")
*/
                addTemperatureServ(keys,values)
                dialog.dismiss()
            }else{
                showErrorMessage(activity!!,activity!!.getString(R.string.api_no_internet))
            }

        })

symone.setText(Constants.symptomlist.get(0).name)
symtwo.setText(Constants.symptomlist.get(1).name)
symthree.setText(Constants.symptomlist.get(2).name)
symfour.setText(Constants.symptomlist.get(3).name)
symfive.setText(Constants.symptomlist.get(4).name)
symsix.setText(Constants.symptomlist.get(5).name)

        symoneImg.setOnClickListener({
            symtomsFun(it.id)
        })
            symtwoImg.setOnClickListener({
            symtomsFun(it.id)
        })
            symthreeImg.setOnClickListener({
            symtomsFun(it.id)
        })
            symfourImg.setOnClickListener({
            symtomsFun(it.id)
        })
            symfiveImg.setOnClickListener({
            symtomsFun(it.id)
        })
            symsixImg.setOnClickListener({
            symtomsFun(it.id)
        })




 //val no_thanks = dialog.findViewById<View>(R.id.call_back_txt) as TextView
//        numOne.setFormatter(NumberPicker.Formatter { i -> String.format("%02d", i) })
//        numTwo.setFormatter(NumberPicker.Formatter { i -> String.format("%02d", i) })
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
    }

    private fun isInRange(a: Int, b: Int, c: Int): Boolean {
        return if (b > a) c >= a && c <= b else c >= b && c <= a
    }

    fun updateDialog(id: String,temp:String,tempcels:String,des:String,symtom:String) {

        Log.d("tag","id:"+id+"\ntemp:"+temp+"\ncel:"+tempcels+"\ndes:"+des)

        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.layout_check_temp_dialog)
        val value = Date()
        val dateFormatter = SimpleDateFormat("hh-mm-aa")
        val dateFormatterAPI = SimpleDateFormat("hh:mm:ss")

//        dateFormatter.timeZone = TimeZone.getDefault()
//        dateFormatterAPI.timeZone = TimeZone.getDefault()

        var ct = dateFormatter.format(value)
        var ctAPI = dateFormatterAPI.format(value)

        var split = ct.split("-")
        var l = split.size


        val msgTxt = dialog.findViewById<View>(R.id.currentTimeTxt) as TextView
        val timeunitTxt = dialog.findViewById<View>(R.id.timeUnitTxt) as TextView
        val tempunitTxt = dialog.findViewById<View>(R.id.tempUnitTxt) as TextView
        val descTxt = dialog.findViewById<View>(R.id.commentsTxt) as EditText
msgTxt.setText(split[0]+" "+split[1])
        timeunitTxt.setText(split[2])

//        val numOne = dialog.findViewById<View>(R.id.numberOnepick) as NumberPicker
//        val numTwo = dialog.findViewById<View>(R.id.numberTwopick) as NumberPicker

        val numOne = dialog.findViewById<View>(R.id.tempOneEdt) as EditText
        val numTwo = dialog.findViewById<View>(R.id.tempTwoEdt) as EditText
        numOne.requestFocus()

        val celMx = Commonutils.getString(pref!!,Constants.CELSIUS_END)
        val celMn = Commonutils.getString(pref!!,Constants.CELSIUS_START)
        val TemMn = Commonutils.getString(pref!!,Constants.TEMP_START)
        val TemMx = Commonutils.getString(pref!!,Constants.TEMP_END)


        val values =
            arrayOf("00", "10", "20", "30", "40", "50", "60", "70", "80", "90")

//        numTwo.displayedValues = values
//        numTwo.maxValue = 9
//        numTwo.minValue = 0
        var c = tempcels.replace(" °C","")
        var t = temp.replace(" °F","")
        var cels = c.toFloat()
        var temps = t.toFloat()
        var Min_One_edt = 0
        var Max_One_edt = 0

        if(Commonutils.getBoolean(pref!!,Constants.CELSIUS_OR_TEMP)!!) {
            Min_One_edt = celMn!!.toInt()
            Max_One_edt = celMx!!.toInt()

            var sp = ""+cels
            if(sp.contains(".")) {
                var spt = sp.split(".")
                numOne.setText(spt[0],TextView.BufferType.EDITABLE)
                numTwo.setText(spt[1],TextView.BufferType.EDITABLE)
            }else{
                numOne.setText(cels.toString(),TextView.BufferType.EDITABLE)
                numTwo.setText("0")
            }
            tempunitTxt.setText(" °C")
        }else{
            Min_One_edt = TemMn!!.toInt()
            Max_One_edt = TemMx!!.toInt()

            var sp = ""+temps
            if(sp.contains(".")) {
                var spt = sp.split(".")
                numOne.setText(spt[0],TextView.BufferType.EDITABLE)
                numTwo.setText(spt[1],TextView.BufferType.EDITABLE)
            }else{
                numOne.setText(temps.toString(),TextView.BufferType.EDITABLE)
                numTwo.setText("0")
            }
            tempunitTxt.setText(" °F")
        }
SymtomId.clear()
        SymtomId.append(symtom)

        var symone = dialog.findViewById<View>(R.id.symoneTxt) as TextView
        symoneImg = dialog.findViewById<View>(R.id.check_temp_symone) as ImageView
        var symtwo = dialog.findViewById<View>(R.id.symtwoTxt) as TextView
        symtwoImg = dialog.findViewById<View>(R.id.check_temp_symtwo) as ImageView
        var symthree = dialog.findViewById<View>(R.id.symthreeTxt) as TextView
        symthreeImg = dialog.findViewById<View>(R.id.check_temp_symthree) as ImageView
        var symfour = dialog.findViewById<View>(R.id.symfourTxt) as TextView
        symfourImg = dialog.findViewById<View>(R.id.check_temp_symfour) as ImageView
        var symfive = dialog.findViewById<View>(R.id.symfiveTxt) as TextView
        symfiveImg = dialog.findViewById<View>(R.id.check_temp_symfive) as ImageView
        var symsix = dialog.findViewById<View>(R.id.symsixTxt) as TextView
        symsixImg = dialog.findViewById<View>(R.id.check_temp_symsix) as ImageView


        var cancel = dialog.findViewById<View>(R.id.btn_check_cancel) as Button
        var submit = dialog.findViewById<View>(R.id.btn_check_sumbit) as Button

        cancel.setOnClickListener({
            dialog.dismiss()
        })
descTxt.setText(""+des)
        submit.setOnClickListener({

            var vv = numOne.text.toString()
            Log.d("tag","val is with range:"+isInRange(Min_One_edt, Max_One_edt, vv.toInt()))
            var isRange = isInRange(Min_One_edt, Max_One_edt, vv.toInt())
            if(!isRange){
                MDToast.makeText(context,"Temperature value should be minimum $Min_One_edt and maximum $Max_One_edt")
                return@setOnClickListener
            }

            var time = ctAPI
            var temp_type = 1
            if(Commonutils.getBoolean(pref!!,Constants.CELSIUS_OR_TEMP)!!){
                temp_type = 2
            }
            var value = numOne.text.toString()+"."+numTwo.text.toString()
            var symtom = SymtomId
            var desc = descTxt.text.toString().trim()


            val keys = ArrayList<String>()
            val values = ArrayList<String>()

            keys.add("id")
            values.add(""+id)
            keys.add("checkup_time")
            values.add(""+time)
            keys.add("temerature_type")
            values.add(""+temp_type)
            keys.add("temperature")
            values.add(""+value)
            keys.add("symptoms_id")
            values.add(""+symtom)
            keys.add("health_description")
            values.add(""+desc)
            val params = GetJsonParams()
            var p: String? = null
            try {
                p = params.conver_Json(keys, values)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            if(NetworkUtils.isNetWorkAvailable(activity!!)){

                var token = Commonutils.getString(pref!!,Constants.Authorize)
API_TYPE = API_EDIT
               /* DataBindingService(
                    activity,
                    this,
                    APIUtils.ADD_TEMPERATURE,
                    ""+p,
                    APIUtils.POST,token
                ).execute("")
*/
                editTemperatureServ(keys,values)
                dialog.dismiss()
            }else{
                showErrorMessage(activity!!,activity!!.getString(R.string.api_no_internet))
            }
        })

symone.setText(Constants.symptomlist.get(0).name)
symtwo.setText(Constants.symptomlist.get(1).name)
symthree.setText(Constants.symptomlist.get(2).name)
symfour.setText(Constants.symptomlist.get(3).name)
symfive.setText(Constants.symptomlist.get(4).name)
symsix.setText(Constants.symptomlist.get(5).name)

        symoneImg.setOnClickListener({
            symtomsFun(it.id)
        })
            symtwoImg.setOnClickListener({
            symtomsFun(it.id)
        })
            symthreeImg.setOnClickListener({
            symtomsFun(it.id)
        })
            symfourImg.setOnClickListener({
            symtomsFun(it.id)
        })
            symfiveImg.setOnClickListener({
            symtomsFun(it.id)
        })
            symsixImg.setOnClickListener({
            symtomsFun(it.id)
        })




 //val no_thanks = dialog.findViewById<View>(R.id.call_back_txt) as TextView
//        numOne.setFormatter(NumberPicker.Formatter { i -> String.format("%02d", i) })
//        numTwo.setFormatter(NumberPicker.Formatter { i -> String.format("%02d", i) })
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
    }

    private fun editTemperatureServ(keys: ArrayList<String>, values: ArrayList<String>) {
        var token = Commonutils.getString(pref!!,Constants.Authorize)
        val client = PostDataHelperWithAsyncHttpClient()
        client.post(
            keys,
            values,
            activity!!,
            APIUtils.UPDATE_HISTORY,""+token,
            object : AsyncHttpResponseHandler() {


                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray
                ) {
                    run {
                        val response = String(responseBody)

                        onTaskComplete(200, response)

                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray,
                    error: Throwable
                ) {

                }
            })
    }

    private fun addTemperatureServ(keys: ArrayList<String>, values: ArrayList<String>) {
        var token = Commonutils.getString(pref!!,Constants.Authorize)
        val client = PostDataHelperWithAsyncHttpClient()
        client.post(
            keys,
            values,
            activity!!,
            APIUtils.ADD_TEMPERATURE,""+token,
            object : AsyncHttpResponseHandler() {


                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray
                ) {
                    run {
                        val response = String(responseBody)

                        onTaskComplete(200, response)

                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray,
                    error: Throwable
                ) {

                }
            })
    }

    fun symtomsFun(id:Int){

        symfiveImg.alpha    = 1f
        symfourImg.alpha    = 1f
        symthreeImg.alpha   = 1f
        symtwoImg.alpha     = 1f
        symoneImg.alpha     = 1f

        when(id){
            R.id.check_temp_symone->{
                symfiveImg.alpha = 0.5f
                symfourImg.alpha = 0.5f
                symthreeImg.alpha = 0.5f
                symtwoImg.alpha = 0.5f
                symsixImg.alpha = 0.5f

                addSymtoms(Constants.symptomlist.get(0).id)

            }
            R.id.check_temp_symfive -> {
                addSymtoms(Constants.symptomlist.get(4).id)
            }
            R.id.check_temp_symfour -> {
                addSymtoms(Constants.symptomlist.get(3).id)
            }
            R.id.check_temp_symsix -> {
                addSymtoms(Constants.symptomlist.get(5).id)
            }
            R.id.check_temp_symthree -> {
                addSymtoms(Constants.symptomlist.get(2).id)
            }
            R.id.check_temp_symtwo -> {
                addSymtoms(Constants.symptomlist.get(1).id)
            }
        }
    }

    private fun addSymtoms(str: String?) {
//        val chars: CharArray = str!!.toCharArray()

        var l = SymtomId.length
        var repeated: Boolean = false

        for (j in 0 until l){
            if(SymtomId[j].toString() == str){
                repeated = true
                break
            }
        }

        if(!repeated){

            if(l>0) {
                SymtomId.append(","+str)
            }else{
                SymtomId.append(str)
            }
            Log.d("tag","Symtoms id:"+SymtomId.length+" == "+SymtomId.toString()+" == "+str)
        }
    }

/*

    fun setup(view:View,inflater: LayoutInflater){
var dyLinear = view.findViewById<LinearLayout>(R.id.dynamic_linear)
        dyLinear.removeAllViews()
        for (i in 0 until 5) {
            var v = inflater.inflate(R.layout.layout_child_health_history, null, false)
//            var p = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,R.dimen._10sdp)
//            v.layoutParams = p
            dyLinear.addView(v)
        }
//view.findViewById<TextView>(R.id.).set("new "+Commonutils.getString(pref!!,Constants.USERNAME))

    }
*/

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showHealth_Reclyer.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(activity)
            // set the custom adapter to the RecyclerView
//            adapter = RecyclerAdapter
        }

        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true)
//showHealth_Reclyer.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true))

        showHealth_Reclyer.layoutManager = linearLayoutManager

        val dividerItemDecorationtemplate =
            DividerItemDecoration(
                showHealth_Reclyer.context,
                DividerItemDecoration.HORIZONTAL
            )
        dividerItemDecorationtemplate.setDrawable(
            activity?.let {
                ContextCompat.getDrawable(
                    it,
                    com.jamaica.temperature.R.drawable.divider
                )
            }!!
        )

        showHealth_Reclyer.addItemDecoration(dividerItemDecorationtemplate)

        showHealth_Reclyer.scrollToPosition(Constants.healthHistoryList.size-1)


    }

//    fun setUpUI(){
//        var switchMode = Commonutils.getBoolean(pref!!,Constants.CELSIUS_OR_TEMP)
//        var range = Constants.rangelist
//        if(switchMode!!){
//            root.findViewById<TextView>(R.id.lasttimeTxt).setText(Commonutils.getString(pref!!,Constants.LAST_CELSIUS))
//            root.findViewById<TextView>(R.id.temponeTxt).setText(range[0].celsius)
//            root.findViewById<TextView>(R.id.temponevalueTxt).setText(range[0].value)
//            root.findViewById<TextView>(R.id.temptwoTxt).setText(range[1].celsius)
//            root.findViewById<TextView>(R.id.temptwovalueTxt).setText(range[1].value)
//            root.findViewById<TextView>(R.id.tempthreeTxt).setText(range[2].celsius)
//            root.findViewById<TextView>(R.id.tempthreevalueTxt).setText(range[2].value)
//            root.findViewById<TextView>(R.id.tempfourTxt).setText(range[3].celsius)
//            root.findViewById<TextView>(R.id.tempfourvalueTxt).setText(range[3].value)
//
//        }else{
//            root.findViewById<TextView>(R.id.lasttimeTxt).setText(Commonutils.getString(pref!!,Constants.LAST_TEMP))
//
//            root.findViewById<TextView>(R.id.temponeTxt).setText(range[0].temperature)
//            root.findViewById<TextView>(R.id.temponevalueTxt).setText(range[0].value)
//            root.findViewById<TextView>(R.id.temptwoTxt).setText(range[1].temperature)
//            root.findViewById<TextView>(R.id.temptwovalueTxt).setText(range[1].value)
//            root.findViewById<TextView>(R.id.tempthreeTxt).setText(range[2].temperature)
//            root.findViewById<TextView>(R.id.tempthreevalueTxt).setText(range[2].value)
//            root.findViewById<TextView>(R.id.tempfourTxt).setText(range[3].temperature)
//            root.findViewById<TextView>(R.id.tempfourvalueTxt).setText(range[3].value)
//        }
//
//
//    }

    fun initobserver(){

            if(NetworkUtils.isNetWorkAvailable(activity!!)){

                // https://dev.amberconnect.com/AmberConnectApp/public/v2.3/user/updatelanguage?
                // Platform=Android&DeviceId=5643b21fbac7cbef&DeviceOffset=19800&DeviceDateTime=2019-09-13 11:45:06
                // &FleetId=959506&MultiUserKey=Y&Id=CI329WGZVJ&UserType=ADMIN&DriverLogin=N&DeviceTimezone=GMT+05:30&Version=3.4
                // &AmberAuthToken=C3FP5M8DB1G2&Vin=C3FP5M8DB1G2&UserToken=46C1WUA2X9&Language=es
             /*   val keys = java.util.ArrayList<String?>()
                val values = ArrayList<String>()

                keys.add("Authorization")
                values.add(""+Commonutils.getString(pref!!,Constants.Authorize))
                val params = GetJsonParams()
                var p: String? = null
                try {
                    p = params.conver_Json(keys, values)
                } catch (e: Exception) {
                    e.printStackTrace()
                }*/
                var token = Commonutils.getString(pref!!,Constants.Authorize)
                Log.d("tag","dashboard api token:"+token)
API_TYPE = API_DASHBOARD
                DataBindingService(
                    activity,
                    this,
                    APIUtils.DASHBOARD + "?" + APIUtils.TIME_STAMP+"=${(System.currentTimeMillis())}",
                    "",
                    APIUtils.GET,token
                ).execute("")

            }else{
                showErrorMessage(activity!!,activity!!.getString(R.string.api_no_internet))
            }

    }

    override fun onTaskComplete(code:Int, result: String?) {
        if(code == 200) {

            if(API_TYPE == API_ADDTEMP || API_TYPE == API_EDIT || API_TYPE == API_DELETE){
                Log.d("tag","Add temperatur response:"+result)
initobserver()
            }
            if (API_TYPE == API_DASHBOARD) {
                var obj = JSONObject(result)

                var data = Commonutils.hasJsonForKey(obj, "data")
                var lastTemp = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "health_details"),
                    "last_temp_id"
                )
                var lastCels = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "health_details"),
                    "last_celsius_id"
                )
                var lastDate = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "health_details"),
                    "last_updated"
                )


                Commonutils.saveString(pref!!, Constants.LAST_TEMP, lastTemp)
                Commonutils.saveString(pref!!, Constants.LAST_CELSIUS, lastCels)
                Commonutils.saveString(pref!!, Constants.LAST_UPDATE_DATE, lastDate)

                var id =
                    Commonutils.hasStringForKey(
                        Commonutils.hasJsonForKey(data, "user_details"),
                        "id"
                    )
                var name =
                    Commonutils.hasStringForKey(
                        Commonutils.hasJsonForKey(data, "user_details"),
                        "name"
                    )
                var username = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "username"
                )
                var lastname = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "lastname"
                )
                var address = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "address"
                )
                var gender = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "gender"
                )
                var dob =
                    Commonutils.hasStringForKey(
                        Commonutils.hasJsonForKey(data, "user_details"),
                        "dob"
                    )
                var profile_image = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "profile_image"
                )
                var dialing_code_id = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "dialing_code_id"
                )
                var mobile_number = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "mobile_number"
                )
                var email = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "email"
                )
                var patient_id = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "patient_id"
                )
                var status = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "status"
                )
                var user_type = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "user_type"
                )
                var staff_id = Commonutils.hasStringForKey(
                    Commonutils.hasJsonForKey(data, "user_details"),
                    "staff_id"
                )

                Commonutils.saveString(pref!!, Constants.USERID, id)
                Commonutils.saveString(pref!!, Constants.NAME, name)
                Commonutils.saveString(pref!!, Constants.LASTNAME, lastname)
                Commonutils.saveString(pref!!, Constants.USERNAME, username)
                Commonutils.saveString(pref!!, Constants.ADDRESS, address)
                Commonutils.saveString(pref!!, Constants.GENDER, gender)
                Commonutils.saveString(pref!!, Constants.DOB, dob)
                Commonutils.saveString(pref!!, Constants.USERIMAGE, profile_image)
                Commonutils.saveString(pref!!, Constants.MOBILE, mobile_number)
                Commonutils.saveString(pref!!, Constants.EMAIL, email)
                Commonutils.saveString(pref!!, Constants.PATIENTID, patient_id)
                Commonutils.saveString(pref!!, Constants.STAFFID, staff_id)
                Commonutils.saveString(pref!!, Constants.STATUS, status)
//        Commonutils.saveString(pref!!,Constants.LASTNAME,status)

                root.findViewById<TextView>(R.id.nameTxt).setText(name)
                if (patient_id.length > 0) {
                    root.findViewById<TextView>(R.id.patientTxt).setText(patient_id)
                } else {
                    root.findViewById<TextView>(R.id.patientTxt).setText(staff_id)
                }

                if (Commonutils.getBoolean(pref!!, Constants.CELSIUS_OR_TEMP)!!) {
                    root.findViewById<TextView>(R.id.lasttimeTxt).setText(lastCels)
                } else {
                    root.findViewById<TextView>(R.id.lasttimeTxt).setText(lastTemp)
                }
                root.findViewById<TextView>(R.id.lastdateTxt).setText(lastDate)

                var arry = Commonutils.hasArrayForKey(data, "health_history")
                var l = arry.length()
                Constants.healthHistoryList.clear()
                if(l<5){

                }else{
                    l = 5
                }
                for (i in 0 until l) {
                    var obj1 = arry.getJSONObject(i)
                    var health_id = Commonutils.hasStringForKey(obj1, "id")
                    var symptom_id = Commonutils.hasStringForKey(obj1, "symptoms_id")
                    var health_description = Commonutils.hasStringForKey(obj1, "health_description")
                    var temperature_value = Commonutils.hasStringForKey(obj1, "temperature_value")
                    var temperature_celsius =
                        Commonutils.hasStringForKey(obj1, "temperature_celsius")
                    var temperature_lbl = Commonutils.hasStringForKey(obj1, "temperature_lbl")
                    var checkup_time = Commonutils.hasStringForKey(obj1, "checkup_time")
                    var added_day = Commonutils.hasStringForKey(obj1, "added_day")
                    var added_time = Commonutils.hasStringForKey(obj1, "added_time")
                    var hData = HealthHistoryData(
                        health_id,
                        symptom_id,
                        health_description,
                        temperature_value,
                        temperature_celsius,
                        temperature_lbl,
                        checkup_time,
                        added_day,
                        added_time
                    )
                    Constants.healthHistoryList.add(hData)
                }

                var range = Constants.rangelist
                root.findViewById<TextView>(R.id.temponeTxt).setText(range[0].temperature)
                root.findViewById<TextView>(R.id.temponevalueTxt).setText(range[0].value)
                root.findViewById<TextView>(R.id.temptwoTxt).setText(range[1].temperature)
                root.findViewById<TextView>(R.id.temptwovalueTxt).setText(range[1].value)
                root.findViewById<TextView>(R.id.tempthreeTxt).setText(range[2].temperature)
                root.findViewById<TextView>(R.id.tempthreevalueTxt).setText(range[2].value)
                root.findViewById<TextView>(R.id.tempfourTxt).setText(range[3].temperature)
                root.findViewById<TextView>(R.id.tempfourvalueTxt).setText(range[3].value)


                var reve = Collections.reverse(Constants.healthHistoryList)
                if(healthhistoryAdpter!=null){
                    healthhistoryAdpter?.notifyDataSetChanged()
                }else {
                    healthhistoryAdpter =
                        RecyclerViewAdapter2(
                            R.layout.layout_child_health_history,
                            this,
                            Constants.healthHistoryList
                        )

                    showHealth_Reclyer.adapter = healthhistoryAdpter
                }
                showHealth_Reclyer.scrollToPosition(Constants.healthHistoryList.size - 1)
            }
        }

    }

    override fun onBindData(dataBinding: Any, model: Any, position: Int) {
        dataBinding as LayoutChildHealthHistoryBinding
        model as HealthHistoryData
        dataBinding.lvHealthDate.setText(model.addedday)
        dataBinding.lvHealthTime.setText(model.addedtime)
        var l = Constants.rangelist.size
        var healthTempColor = activity?.resources?.getColor(R.color.low_temp)
        for (i in 0 until l){
            var d = Constants.rangelist.get(i)
            var Tempsplit = d.temperature?.split("-")
            var Celsplit = d.celsius?.split("-")

            var TempMin = Tempsplit?.get(0)?.toFloat()
            var TempMax = Tempsplit?.get(1)?.toFloat()

            var CelMin = Celsplit?.get(0)?.toFloat()
            var CelMax = Celsplit?.get(1)?.toFloat()

            var c = model.tempcels.replace(" °C","")
            var t = model.tempvalue.replace(" °F","")
            var cels = c.toFloat()
            var temps = t.toFloat()

//            var c =cels.coerceIn(CelMin,CelMax)

                when(d.id?.toInt()){
                    1 -> {
                        Log.d ("Tag","cels 1:"+cels)
                        if(cels >= CelMin!!){
                            if(cels <= CelMax!!) {
                                healthTempColor = activity?.resources?.getColor(R.color.low_temp)
                            }
                        }

                        if(temps>=TempMin!!) {
                            if(temps <= TempMax!!) {
                                healthTempColor = activity?.resources?.getColor(R.color.low_temp)
                            }
                        }
                    }
                    2 -> {
                        Log.d ("Tag","cels 2:"+cels)

                        if(cels >= CelMin!!){
                            if(cels <= CelMax!!){
                                healthTempColor = activity?.resources?.getColor(R.color.normal_temp)
                            }
                        }

                        if(temps>=TempMin!!) {
                            if(temps <= TempMax!!) {
                                healthTempColor = activity?.resources?.getColor(R.color.normal_temp)
                            }
                        }
                    }
                    3 -> {
                        Log.d ("Tag","cels3:"+cels)

                        if(cels >= CelMin!!){
                            if(cels <= CelMax!!){
                                healthTempColor = activity?.resources?.getColor(R.color.fever_temp)
                            }
                        }

                        if(temps>=TempMin!!) {
                            if(temps <= TempMax!!) {
                                healthTempColor = activity?.resources?.getColor(R.color.fever_temp)
                            }
                        }
                    }
                    4 -> {
                        Log.d ("Tag","cels4:"+cels)

                        if(cels >= CelMin!!){
//                            if(cels <= CelMax!!){
                                healthTempColor = activity?.resources?.getColor(R.color.high_fever_temp)
//                            }
                        }

                        if(temps>=TempMin!!) {

                                healthTempColor = activity?.resources?.getColor(R.color.high_fever_temp)

                        }
                    }
                }



        }
        if (Commonutils.getBoolean(pref!!,Constants.CELSIUS_OR_TEMP)!!) {
            dataBinding.lvHealthTemp.setText(model.tempcels)
        }else {
            dataBinding.lvHealthTemp.setText(model.tempvalue)
        }
        dataBinding.lvHealthTemp.setTextColor(healthTempColor!!)

        dataBinding.lvHealthStatus.setText(model.templbl)

        dataBinding.editLinear.setOnClickListener({
            dataBinding.editLinear.visibility = View.GONE
        })
dataBinding.editImg.setOnClickListener({
    Log.d("tag","data module edit clicked:")
    dataBinding.editLinear.visibility = View.GONE
    editTodayTemp(model.id,model.tempvalue,model.tempcels,model.descr,model.symptom)
})
dataBinding.deleteImg.setOnClickListener({
    Log.d("tag","data module delete clicked:")
    dataBinding.editLinear.visibility = View.GONE
    deleteTodayTemp(model.id)
})
    }

    private fun deleteTodayTemp(id: String) {

        val keys = ArrayList<String>()
        val values = ArrayList<String>()

        keys.add("id")
        values.add(""+id)
        val params = GetJsonParams()
        var p: String? = null
        try {
            p = params.conver_Json(keys, values)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if(NetworkUtils.isNetWorkAvailable(activity!!)){

            var token = Commonutils.getString(pref!!,Constants.Authorize)
            API_TYPE = API_DELETE
            deleteTemperatureServ(keys,values)
        }else{
            showErrorMessage(activity!!,activity!!.getString(R.string.api_no_internet))
        }
    }

    private fun deleteTemperatureServ(keys: ArrayList<String>, values: ArrayList<String>) {
        var token = Commonutils.getString(pref!!,Constants.Authorize)
        val client = PostDataHelperWithAsyncHttpClient()
        client.post(
            keys,
            values,
            activity!!,
            APIUtils.DELETE_HISTORY,""+token,
            object : AsyncHttpResponseHandler() {


                override fun onSuccess(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray
                ) {
                    run {
                        val response = String(responseBody)

                        onTaskComplete(200, response)

                    }
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<Header>,
                    responseBody: ByteArray,
                    error: Throwable
                ) {

                }
            })
    }

    private fun editTodayTemp(id: String, tempvalue: String, tempcels: String,des: String,symtom: String) {
        SymtomId  = java.lang.StringBuilder()
        updateDialog(id,tempvalue,tempcels,des,symtom)
    }

    override fun onItemClick(view :View,model: Any, position: Int) {
        model as HealthHistoryData
        val value = Date()
        val dateFormatter = SimpleDateFormat("dd MMM yy")
        var ct = dateFormatter.format(value)
        Log.d("tag","current date :"+ct+"\n "+model.addedday)
        if(ct.equals(model.addedday)) {
            view.findViewById<View>(R.id.editLinear).visibility = View.VISIBLE
        }
    }
}