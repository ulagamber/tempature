package com.jamaica.temperature.ui.home

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jamaica.temperature.R
import com.jamaica.temperature.extensions.showErrorMessage
import com.jamaica.temperature.service.DataBindingService
import com.jamaica.temperature.ui.data.DashboardData
import com.jamaica.temperature.ui.data.UserData
import com.jamaica.temperature.utils.APIUtils
import com.jamaica.temperature.utils.NetworkUtils
import java.util.ArrayList

class HomeViewModel : ViewModel() {

    val name by lazy { ObservableField<String>("") }
    val logo by lazy { ObservableField<String>("") }
    val lastname by lazy { ObservableField<String>("") }
    val patientid by lazy { ObservableField<String>("") }
    val lasttimetemp by lazy { ObservableField<String>("") }
    val lastupdate by lazy { ObservableField<String>("") }
    val tempone by lazy { ObservableField<String>("") }
    val temptwo by lazy { ObservableField<String>("") }
    val tempthree by lazy { ObservableField<String>("") }
    val tempfour by lazy { ObservableField<String>("") }
    val temponevalue by lazy { ObservableField<String>("") }
    val temptwovalue by lazy { ObservableField<String>("") }
    val tempthreevalue by lazy { ObservableField<String>("") }
    val tempfourvalue by lazy { ObservableField<String>("") }
    val isceliciuson by lazy { ObservableBoolean(false) }


}