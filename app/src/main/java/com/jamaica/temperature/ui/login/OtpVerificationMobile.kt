package com.jamaica.temperature.ui.login

import android.app.Activity
import android.app.Service
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.jamaica.temperature.R
import com.jamaica.temperature.service.PostDataHelperWithAsyncHttpClient
import com.jamaica.temperature.utils.APIUtils
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class OtpVerificationMobile : Activity(), View.OnFocusChangeListener, View.OnKeyListener,
    TextWatcher {

    private var mPinFirstDigitEditText: EditText? = null
    private var mPinSecondDigitEditText: EditText? = null
    private var mPinThirdDigitEditText: EditText? = null
    private var mPinForthDigitEditText: EditText? = null
    private var mPinHiddenEditText: EditText? = null
    private var btn_login2: TextView? = null
    internal lateinit var otp_mobile: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.otp_verification)

        mPinFirstDigitEditText = findViewById(R.id.pin_first_edittext)
        mPinSecondDigitEditText = findViewById(R.id.pin_second_edittext)
        mPinThirdDigitEditText = findViewById(R.id.pin_third_edittext)
        mPinForthDigitEditText = findViewById(R.id.pin_forth_edittext)
        mPinHiddenEditText = findViewById(R.id.pin_hidden_edittext)
        btn_login2 = findViewById(R.id.btn_login2)

        setPINListeners()

        btn_login2!!.setOnClickListener {
            otpVerificationAcc(otp_mobile)
        }

    }

    private fun otpVerificationAcc(
        otp: String

    ) {
        try {
/*
            if (!mAmberUtils.isDataConnected(applicationContext)) {
                //                no_internet_lay.setVisibility(View.VISIBLE);
                mAmberUtils.InternetAlert(this@Activity_Signup)
            } else {
                no_internet_lay.visibility = View.GONE
                pd = null
                pd = ProgressDialog(this@Activity_Signup)
                pd!!.setCancelable(false)
                pd!!.setMessage(resources.getString(R.string.str_Loading_dot))
                pd!!.show()
            }*/


            val keys = java.util.ArrayList<String>()
            val values = java.util.ArrayList<String>()

            keys.add("dialing_code_id")
            values.add("1")
            keys.add("otp_field")
            values.add("3344556")
            keys.add("otp")
            values.add(otp)
            keys.add("login")
            values.add("1")




            val client = PostDataHelperWithAsyncHttpClient()
            client.post(
                keys,
                values,
                applicationContext,
                APIUtils.VERIFY_OTP,"",
                object : AsyncHttpResponseHandler() {


                    override fun onSuccess(
                        statusCode: Int,
                        headers: Array<Header>,
                        responseBody: ByteArray
                    ) {
                        run {
                            val response = String(responseBody)
                            if (response != null) {
                                Log.d("TagResponse",response);
                                var obj = JSONObject(response)
                            }
                        }
                    }

                    override fun onFailure(
                        statusCode: Int,
                        headers: Array<Header>,
                        responseBody: ByteArray,
                        error: Throwable
                    ) {

                    }
                })
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        val id = v.id
        when (id) {
            R.id.pin_first_edittext -> if (hasFocus) {
                setFocus(mPinHiddenEditText)
                showSoftKeyboard(mPinHiddenEditText)
            }

            R.id.pin_second_edittext -> if (hasFocus) {
                setFocus(mPinHiddenEditText)
                showSoftKeyboard(mPinHiddenEditText)
            }

            R.id.pin_third_edittext -> if (hasFocus) {
                setFocus(mPinHiddenEditText)
                showSoftKeyboard(mPinHiddenEditText)
            }

            R.id.pin_forth_edittext -> if (hasFocus) {
                setFocus(mPinHiddenEditText)
                showSoftKeyboard(mPinHiddenEditText)
            }



            else -> {
            }
        }
    }


    override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            val id = v.id
            when (id) {
                R.id.pin_hidden_edittext -> if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (mPinHiddenEditText!!.text.length == 4)
                        mPinForthDigitEditText!!.setText("")
                    else if (mPinHiddenEditText!!.text.length == 3)
                        mPinThirdDigitEditText!!.setText("")
                    else if (mPinHiddenEditText!!.text.length == 2)
                        mPinSecondDigitEditText!!.setText("")
                    else if (mPinHiddenEditText!!.text.length == 1)
                        mPinFirstDigitEditText!!.setText("")



                    if (mPinHiddenEditText!!.length() > 0)
                        mPinHiddenEditText!!.setText(
                            mPinHiddenEditText!!.text.subSequence(
                                0,
                                mPinHiddenEditText!!.length() - 1
                            )
                        )

                    return true
                }

                else ->
                    //                    Log.d("oiohugsfyibefore",mPinHiddenEditText.getText().toString());

                    return false
            }

        }

        //        Log.d("oiohugsfyiafter",mPinHiddenEditText.getText().toString());

        return false
    }


    override fun afterTextChanged(s: Editable) {
        if (s.toString().length == 4) {
            val str = s.toString()
            otp_mobile = str.substring(0, 4)

            val otp_for_mobile = Integer.parseInt(otp_mobile)
            Toast.makeText(applicationContext,otp_mobile,Toast.LENGTH_LONG).show()
        }

    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {


    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        setDefaultPinBackground(mPinFirstDigitEditText)
        setDefaultPinBackground(mPinSecondDigitEditText)
        setDefaultPinBackground(mPinThirdDigitEditText)
        setDefaultPinBackground(mPinForthDigitEditText)





        if (s.length == 0) {
            setFocusedPinBackground(mPinFirstDigitEditText)
            mPinFirstDigitEditText!!.setText("")
        } else if (s.length == 1) {
            setFocusedPinBackground(mPinSecondDigitEditText)
            mPinFirstDigitEditText!!.setText(s[0] + "")
            mPinSecondDigitEditText!!.setText("")
            mPinThirdDigitEditText!!.setText("")
            mPinForthDigitEditText!!.setText("")


        } else if (s.length == 2) {
            setFocusedPinBackground(mPinThirdDigitEditText)
            mPinSecondDigitEditText!!.setText(s[1] + "")
            mPinThirdDigitEditText!!.setText("")
            mPinForthDigitEditText!!.setText("")

        } else if (s.length == 3) {
            setFocusedPinBackground(mPinForthDigitEditText)
            mPinThirdDigitEditText!!.setText(s[2] + "")
            mPinForthDigitEditText!!.setText("")


        } else if (s.length == 4) {
            mPinForthDigitEditText!!.setText(s[3] + "")

        }

    }

    fun setFocus(editText: EditText?) {
        if (editText == null)
            return

        editText.isFocusable = true
        editText.isFocusableInTouchMode = true
        editText.requestFocus()
    }

    fun showSoftKeyboard(editText: EditText?) {
        if (editText == null)
            return

        val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editText, 0)
    }
    private fun setPINListeners() {
        mPinHiddenEditText!!.addTextChangedListener(this)

        mPinFirstDigitEditText!!.onFocusChangeListener = this
        mPinSecondDigitEditText!!.onFocusChangeListener = this
        mPinThirdDigitEditText!!.onFocusChangeListener = this
        mPinForthDigitEditText!!.onFocusChangeListener = this



        mPinFirstDigitEditText!!.setOnKeyListener(this)
        mPinSecondDigitEditText!!.setOnKeyListener(this)
        mPinThirdDigitEditText!!.setOnKeyListener(this)
        mPinForthDigitEditText!!.setOnKeyListener(this)
        mPinHiddenEditText!!.setOnKeyListener(this)


    }

    private fun setDefaultPinBackground(editText: EditText?) {
        setViewBackground(editText, resources.getDrawable(R.drawable.border_edittext))
    }
    fun setViewBackground(view: View?, background: Drawable?) {
        if (view == null || background == null)
            return

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.background = background
        } else {
            view.setBackgroundDrawable(background)
        }
    }

    private fun setFocusedPinBackground(editText: EditText?) {
        setViewBackground(editText, resources.getDrawable(R.drawable.border_edittext))
    }
}