package com.jamaica.temperature.ui.login

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.jamaica.temperature.R
import com.jamaica.temperature.service.*
import com.jamaica.temperature.utils.APIUtils
import com.jamaica.temperature.utils.Commonutils
import com.jamaica.temperature.utils.Constants
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class RegisterAccount : Activity(), AsyncTaskCompleteListener<String> {

    private var first_name: EditText? = null
    private var last_name: EditText? = null
    private var ph_no: EditText? = null
    private var email_id: EditText? = null
    private var pw: EditText? = null
    private var confirm_pw: EditText? = null
    private var register_acc: TextView? = null
    private var pref: SharedPreferences? = null
    var response: String = ""
    private var pd: ProgressDialog? = null

    private var async_save: AsyncTask<String, Void, Boolean>? =
        null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        pref = getSharedPreferences(Constants.PrefName, Context.MODE_PRIVATE)

        first_name = findViewById(R.id.edt_first_name)
        last_name = findViewById(R.id.edt_last_name)
        ph_no = findViewById(R.id.edt_ph_no)
        email_id = findViewById(R.id.edt_em_id)
        pw = findViewById(R.id.edt_create_pw)
        confirm_pw = findViewById(R.id.edt_confirm_pw)
        register_acc = findViewById(R.id.txt_register)

        register_acc!!.setOnClickListener {
            //            Toast.makeText(applicationContext,"clicked",Toast.LENGTH_LONG).show()

            if(first_name!!.text.toString().equals("")){
                Toast.makeText(applicationContext,resources.getString(R.string.reg_fullname), Toast.LENGTH_LONG).show()
            }else if(last_name!!.text.toString().equals("")){
                Toast.makeText(applicationContext,resources.getString(R.string.reg_fulllastname), Toast.LENGTH_LONG).show()
            }else if(ph_no!!.text.toString().equals("")){
                Toast.makeText(applicationContext,resources.getString(R.string.str_Enter_your_Phone_Number), Toast.LENGTH_LONG).show()
            }else if(ph_no!!.text.length < 7){
                Toast.makeText(applicationContext,resources.getString(R.string.str_Enter_your_Phone_Number_length), Toast.LENGTH_LONG).show()
            }else if(email_id!!.text.toString().equals("")){
                Toast.makeText(applicationContext,resources.getString(R.string.reg_email), Toast.LENGTH_LONG).show()
            }else if (!Commonutils.isValidEmail(email_id!!.text.toString().trim { it <= ' ' })) {
                Toast.makeText(
                    applicationContext,
                    resources.getString(R.string.login_email_valid),
                    Toast.LENGTH_SHORT
                ).show()
            }else if(pw!!.text.toString().equals("")){
                Toast.makeText(applicationContext,resources.getString(R.string.str_Enter_your_password_here), Toast.LENGTH_LONG).show()
            }else if(pw!!.text.length < 7){
                Toast.makeText(applicationContext,resources.getString(R.string.str_Enter_your_password_here_length), Toast.LENGTH_LONG).show()
            }else if(confirm_pw!!.text.toString().equals("")){
                Toast.makeText(applicationContext,resources.getString(R.string.tst_Please_Enter_a_Confirm_Password), Toast.LENGTH_LONG).show()
            }else if(!confirm_pw!!.text.toString().equals(pw!!.text.toString())){
                Toast.makeText(applicationContext,resources.getString(R.string.tst_Please_Check_Your_Confirm_Password), Toast.LENGTH_LONG).show()
            }else{
                if(pw!!.text.toString().equals("").equals(confirm_pw!!.text.toString().equals(""))) {
                    registerAcc(
                        first_name!!.text.toString(),
                        last_name!!.text.toString(),
                        ph_no!!.text.toString(),
                        email_id!!.text.toString(),
                        pw!!.text.toString(),
                        confirm_pw!!.text.toString()
                    )
                }
            }



        }


    }

    fun API_parse() {

        val keys = java.util.ArrayList<String>()
        val values = ArrayList<String>()

        keys.add("name")
        values.add("" + first_name!!.text)
        keys.add("last_name")
        values.add("" + last_name!!.text)
        keys.add("dialing_code_id")
        values.add("1")
        keys.add("mobile_number")
        values.add(""+ ph_no!!.text  )
        keys.add("email")
        values.add(""+email_id!!.text  )
        keys.add("password")
        values.add(""+pw!!.text )
        keys.add("confirm_password")
        values.add("" + confirm_pw!!.text )


        val params = GetJsonParams()
        var p: String? = null
        try {
            p = params.conver_Json(keys, values)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        DataBindingService(
            applicationContext,
            this,
            APIUtils.REGISTER_ACC+ "?",
            p,
            APIUtils.POST
        ).execute("")
    }

    override fun onTaskComplete(code :Int, result: String?) {
        Log.d("tag", "Reg resposne:" + result)
        var json = JSONObject(result)
        var data = Commonutils.hasJsonForKey(json, "data")

    }

    private fun registerAcc(
        FirstName: String,
        LastName: String,
        Phone: String,
        EmailId: String,
        Pw: String,
        ConfirmPw: String
    ) {
        try {
            if (!Commonutils.isDataConnected(applicationContext)) {

            } else {
                pd = null
                pd = ProgressDialog(this@RegisterAccount)
                pd!!.setCancelable(false)
                pd!!.setMessage(resources.getString(R.string.str_Loading_dot))
                pd!!.show()
            }


            val keys = java.util.ArrayList<String>()
            val values = java.util.ArrayList<String>()

            keys.add("name")
            values.add(FirstName)
            keys.add("last_name")
            values.add(LastName)
            keys.add("dialing_code_id")
            values.add("1")
            keys.add("mobile_number")
            values.add(Phone)
            keys.add("email")
            values.add(EmailId)
            keys.add("password")
            values.add(Pw)
            keys.add("confirm_password")
            values.add(ConfirmPw)



            val client = PostDataHelperWithAsyncHttpClient()
            client.post(
                keys,
                values,
                applicationContext,
                APIUtils.REGISTER_ACC,"",
                object : AsyncHttpResponseHandler() {


                    override fun onSuccess(
                        statusCode: Int,
                        headers: Array<Header>,
                        responseBody: ByteArray
                    ) {
                        run {

                            for(i in 0 until headers.size){
                                Log.d("TagResponse","headerName.."+headers.get(i).name);

                                if(headers.get(i).name.equals("Authorization")){
                                    val auth = headers.get(i).value
                                    Log.d("TagResponse","headerAuth.."+auth);
                                    Commonutils.saveString(pref!!, Constants.Authorize, auth)
                                }
                            }
                            val response = String(responseBody)
                            if (response != null) {
                                Log.d("TagResponse",""+response+"header.."+headers.size);
                                var obj = JSONObject(response)
                                var message = obj.getString("message")
                                var data = Commonutils.hasJsonForKey(obj, "data")

                                var otp = data.getString("otp")
                                var otp_count_mobile_number = data.getString( "otp_count_mobile_number")
                                var otp_count_email = data.getString( "otp_count_email")
                                var otp_sent_to = data.getString( "otp_sent_to")

                                var id = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "id")
                                var name = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "name")
                                var lastname = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "last_name")
                                var address = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "address")
                                var gender = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "gender")
                                var dob = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "dob")
                                var profile_image = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "profile_image")
                                var user_dialing_code_id = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "dialing_code_id")
                                var mobile_number = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "mobile_number")
                                var email = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "email")
                                var username = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "username")
                                var patient_id = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "patient_id")
                                var status = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "status")
                                var user_user_type = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "user_type")
                                var user_email_verified_at = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "email_verified_at")
                                var user_mobile_verified_at = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "mobile_verified_at")
                                var staff_id = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "staff_id")
                                var user_is_email_verified_at = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "is_email_verified_at")
                                var user_is_mobile_verified_at = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "is_mobile_verified_at")

                                Commonutils.saveString(pref!!, Constants.USERID, id)
                                Commonutils.saveString(pref!!, Constants.NAME, name)
                                Commonutils.saveString(pref!!, Constants.LASTNAME, lastname)
                                Commonutils.saveString(pref!!, Constants.USERNAME, username)
                                Commonutils.saveString(pref!!, Constants.ADDRESS, address)
                                Commonutils.saveString(pref!!, Constants.GENDER, gender)
                                Commonutils.saveString(pref!!, Constants.DOB, dob)
                                Commonutils.saveString(pref!!, Constants.USERIMAGE, profile_image)
                                Commonutils.saveString(pref!!, Constants.MOBILE, mobile_number)
                                Commonutils.saveString(pref!!, Constants.EMAIL, email)
                                Commonutils.saveString(pref!!, Constants.PATIENTID, patient_id)
                                Commonutils.saveString(pref!!, Constants.STAFFID, staff_id)
                                Commonutils.saveString(pref!!, Constants.STATUS, status)
                                Commonutils.saveString(pref!!, Constants.OTP, otp)
                                Commonutils.saveString(pref!!, Constants.OTP_SENT_TO, otp_sent_to)
                                Commonutils.saveString(pref!!, Constants.DIAL_CODE_ID, user_dialing_code_id)
                                Commonutils.saveString(pref!!, Constants.OTP_COUNT_MOBILE, user_dialing_code_id)
                                Commonutils.saveString(pref!!, Constants.OTP_COUNT_MAIL, otp_count_mobile_number)
                                pd!!.dismiss()
                                Toast.makeText(
                                    applicationContext,
                                    message,
                                    Toast.LENGTH_SHORT
                                ).show()
                                Log.d("TagResponseData",data.toString());
                                finish()
                                val i = Intent(this@RegisterAccount,OtpVerification::class.java)
                                startActivity(i)

                            }
                        }
                    }

                    override fun onFailure(
                        statusCode: Int,
                        headers: Array<Header>,
                        responseBody: ByteArray,
                        error: Throwable
                    ) {
                        pd!!.dismiss()

                        val response = String(responseBody)

                        if (response != null) {
                            Log.d("TagResponseFailure",response)
                            var obj = JSONObject(response)
                            var message = obj.getString("message")
                            Toast.makeText(
                                applicationContext,
                                message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

    }

    private inner class AddSaveTask // TODO Auto-generated constructor stub
        (
        var FirstName: String,
        var LastName: String,
        var Phone: String,
        var EmailId: String,
        var Pw: String,
        var ConfirmPw: String
    ) :
        AsyncTask<String, Void, Boolean>() {
        var keys = java.util.ArrayList<String>()
        var values = java.util.ArrayList<String>()
        var amt: String? = null
        override fun onPreExecute() { // TODO Auto-generated method stub
          /*  mProgressHUD = ProgressHUD.show(
                this@ProfileSettings,
                resources.getString(R.string.str_Loading_dot),
                true,
                true
            )
            mProgressHUD!!.setCancelable(false)*/
        }

        protected override fun doInBackground(vararg params: String): Boolean { // TODO Auto-generated method stub
            try {
                keys.add("name")
                values.add(FirstName)
                keys.add("last_name")
                values.add(LastName)
                keys.add("dialing_code_id")
                values.add("1")
                keys.add("mobile_number")
                values.add(Phone)
                keys.add("email")
                values.add(EmailId)
                keys.add("password")
                values.add(Pw)
                keys.add("confirm_password")
                values.add(ConfirmPw)

                val pdata = PostDataHelper(keys, values, applicationContext)
                response =
                    pdata.postData(APIUtils.REGISTER_ACC.trim({ it <= ' ' }))


                if (response != null) {
//                    val obj = JSONObject(response)


                }
            } catch (e: Exception) { // TODO Auto-generated catch block
                e.printStackTrace()
            }
            return false
        }

        protected override fun onPostExecute(result: Boolean) { // TODO Auto-generated method stub
           /* if (mProgressHUD!!.isShowing == true) {
                mProgressHUD!!.dismiss()
            }
            if (result) {
                Toast.makeText(
                    applicationContext,
                    resources.getString(R.string.tst_Successfully_Updated),
                    Toast.LENGTH_SHORT
                ).show()
                val edit1 = settings!!.edit()
                edit1.putString(mAmberUtils.HELPME_FLAG, HelpMeFlag)
                edit1.commit()
                finish()
            }*/


        }

    }

}