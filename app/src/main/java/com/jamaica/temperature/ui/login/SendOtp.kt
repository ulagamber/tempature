package com.jamaica.temperature.ui.login

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.jamaica.temperature.R
import com.jamaica.temperature.service.PostDataHelperWithAsyncHttpClient
import com.jamaica.temperature.utils.APIUtils
import com.jamaica.temperature.utils.Commonutils
import com.jamaica.temperature.utils.Constants
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class SendOtp : Activity(){

    var otp_field: EditText? = null
    var otp_submit : TextView? = null
    private var pref: SharedPreferences? = null
    var token: String = ""
    private var pd: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.otp_send)

        pref = getSharedPreferences(Constants.PrefName, Context.MODE_PRIVATE)

        token = pref!!.getString(Constants.Authorize,"").toString()
        otp_field = findViewById(R.id.edt_ph_no)
        otp_submit = findViewById(R.id.btn_login2)

        otp_submit!!.setOnClickListener {
            otpSendAcc(otp_field!!.text.toString())
        }
    }

    private fun otpSendAcc(
        otp_field: String

    ) {
        try {
            if (!Commonutils.isDataConnected(applicationContext)) {

            } else {
                pd = null
                pd = ProgressDialog(this@SendOtp)
                pd!!.setCancelable(false)
                pd!!.setMessage(resources.getString(R.string.str_Loading_dot))
                pd!!.show()
            }


            val keys = java.util.ArrayList<String>()
            val values = java.util.ArrayList<String>()

            keys.add("dialing_code_id")
            values.add("1")
            keys.add("otp_field")
            values.add(otp_field)





            val client = PostDataHelperWithAsyncHttpClient()
            client.post(
                keys,
                values,
                applicationContext,
                APIUtils.SEND_OTP,token,
                object : AsyncHttpResponseHandler() {


                    override fun onSuccess(
                        statusCode: Int,
                        headers: Array<Header>,
                        responseBody: ByteArray
                    ) {
                        run {
                            val response = String(responseBody)
                            if (response != null) {
                                Log.d("TagResponse",response);
                                var obj = JSONObject(response)
                                var message = obj.getString("message")

                                pd!!.dismiss()
                                finish()
                                Toast.makeText(
                                    applicationContext,
                                    message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }

                    override fun onFailure(
                        statusCode: Int,
                        headers: Array<Header>,
                        responseBody: ByteArray,
                        error: Throwable
                    ) {
                        pd!!.dismiss()
                        val response = String(responseBody)

                        if (response != null) {
                            Log.d("TagResponseFailure",response)
                            var obj = JSONObject(response)
                            var message = obj.getString("message")
                            Toast.makeText(
                                applicationContext,
                                message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

    }
}