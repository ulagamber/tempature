package com.jamaica.temperature.ui.menu

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.jamaica.temperature.MainActivity
import com.jamaica.temperature.R
import com.jamaica.temperature.service.PostDataHelperWithAsyncHttpClient
import com.jamaica.temperature.utils.APIUtils
import com.jamaica.temperature.utils.Commonutils
import com.jamaica.temperature.utils.Constants
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.activity_login.view.*
import org.json.JSONObject

class ChangePassword : Activity() {
    private var old_pw : EditText? = null
    private var new_pw : EditText? = null
    private var confirm_new_pw : EditText? = null
    private var submit : TextView? = null
    private var pd: ProgressDialog? = null
    private var pref: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.change_password)

        old_pw = findViewById(R.id.old_pw)
        new_pw = findViewById(R.id.new_pw)
        confirm_new_pw = findViewById(R.id.conf_new_pw)
        submit = findViewById(R.id.btn_login2)

        submit!!.setOnClickListener {

            changePw(old_pw!!.text.toString(),new_pw!!.text.toString(),confirm_new_pw!!.text.toString())
        }


    }

    private fun changePw(
        pw: String,
        new_Pw: String,
        confirm_pw: String

    ) {
        try {
            if (!Commonutils.isDataConnected(applicationContext)) {

            } else {
                pd = null
                pd = ProgressDialog(this@ChangePassword)
                pd!!.setCancelable(false)
                pd!!.setMessage(resources.getString(R.string.str_Loading_dot))
                pd!!.show()
            }


            val keys = java.util.ArrayList<String>()
            val values = java.util.ArrayList<String>()

            keys.add("email")
            values.add(pw)
            keys.add("password")
            values.add(new_Pw)
            keys.add("device_type")
            values.add("1")
            keys.add("device_token")
            values.add("")




            val client = PostDataHelperWithAsyncHttpClient()
            client.post(
                keys,
                values,
                applicationContext,
                APIUtils.LOGIN_ACC,"",
                object : AsyncHttpResponseHandler() {


                    override fun onSuccess(
                        statusCode: Int,
                        headers: Array<Header>,
                        responseBody: ByteArray
                    ) {
                        run {
                            val response = String(responseBody)
                            Log.d("TagResponse149",response);

                            if (response != null) {
                                Log.d("TagResponse152",response+" statusCode"+statusCode);
                                var obj = JSONObject(response)
                                var message = obj.getString("message")
                                var data = Commonutils.hasJsonForKey(obj, "data")
                                var error_res = Commonutils.hasJsonForKey(obj, "error")

                                var otp = data.getString("otp")
                                var otp_count_mobile_number = data.getString( "otp_count_mobile_number")
                                var otp_count_email = data.getString( "otp_count_email")
                                var otp_sent_to = data.getString( "otp_sent_to")

                                var id = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "id")
                                var name = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "name")
                                var lastname = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "last_name")
                                var address = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "address")
                                var gender = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "gender")
                                var dob = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "dob")
                                var profile_image = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "profile_image")
                                var user_dialing_code_id = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "dialing_code_id")
                                var mobile_number = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "mobile_number")
                                var email = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "email")
                                var username = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "username")
                                var patient_id = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "patient_id")
                                var status = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "status")
                                var user_user_type = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "user_type")
                                var user_email_verified_at = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "email_verified_at")
                                var user_mobile_verified_at = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "mobile_verified_at")
                                var staff_id = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "staff_id")
                                var user_is_email_verified_at = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "is_email_verified_at")
                                var user_is_mobile_verified_at = Commonutils.hasStringForKey(
                                    Commonutils.hasJsonForKey(data, "user"),
                                    "is_mobile_verified_at")

                                Commonutils.saveString(pref!!, Constants.USERID, id)
                                Commonutils.saveString(pref!!, Constants.NAME, name)
                                Commonutils.saveString(pref!!, Constants.LASTNAME, lastname)
                                Commonutils.saveString(pref!!, Constants.USERNAME, username)
                                Commonutils.saveString(pref!!, Constants.ADDRESS, address)
                                Commonutils.saveString(pref!!, Constants.GENDER, gender)
                                Commonutils.saveString(pref!!, Constants.DOB, dob)
                                Commonutils.saveString(pref!!, Constants.USERIMAGE, profile_image)
                                Commonutils.saveString(pref!!, Constants.MOBILE, mobile_number)
                                Commonutils.saveString(pref!!, Constants.EMAIL, email)
                                Commonutils.saveString(pref!!, Constants.PATIENTID, patient_id)
                                Commonutils.saveString(pref!!, Constants.STAFFID, staff_id)
                                Commonutils.saveString(pref!!, Constants.STATUS, status)
                                Commonutils.saveString(pref!!, Constants.OTP, otp)
                                Commonutils.saveString(pref!!, Constants.OTP_SENT_TO, otp_sent_to)
                                Commonutils.saveString(pref!!, Constants.OTP_COUNT_MOBILE, user_dialing_code_id)
                                Commonutils.saveString(pref!!, Constants.OTP_COUNT_MAIL, otp_count_mobile_number)
                                Commonutils.saveString(pref!!, Constants.DIAL_CODE_ID, otp_count_email)

                                pd!!.dismiss()

                                val i = Intent(applicationContext, MainActivity::class.java)
                                startActivity(i)

                                Toast.makeText(
                                    applicationContext,
                                    message,
                                    Toast.LENGTH_SHORT
                                ).show()

                            }else{
                                Log.d("TagResponse251",response+" statusCode"+statusCode);

                            }
                        }
                    }

                    override fun onFailure(
                        statusCode: Int,
                        headers: Array<Header>,
                        responseBody: ByteArray,
                        error: Throwable
                    ) {

                        pd!!.dismiss()
                        Log.d("TagResponse","error "+error.message
                                +" responsebody "+responseBody+" headers" + headers+" statusCode "+statusCode);
                        if(statusCode == 400){
                            Toast.makeText(
                                applicationContext,
                                "Please check your input once again",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
            Log.d("TagResponse282",e.message);

        }

    }
}