package com.jamaica.temperature.utils

object APIUtils {
    const val GET = 1
    const val POST = 2
    const val BASE_URL = "https://kuyadev.myamberpay.com/temp_dev/temperature_v1_api/public/"

    const val CORE_CONFIG = "api/v1/customers/core-config"
    const val DASHBOARD = "api/v1/customers/dashboard"
    const val REGISTER_ACC = "api/v1/customers/register"
    const val LOGIN_ACC = "api/v1/customers/login"
    const val VERIFY_OTP = "api/v1/customers/verify-otp"
    const val SEND_OTP = "api/v1/customers/send-otp"

    const val ADD_TEMPERATURE = "api/v1/customers/add-temperature"
    const val GET_HISTORY = "api/v1/customers/history"
    const val DELETE_HISTORY = "api/v1/customers/delete-temperature"
    const val UPDATE_HISTORY = "api/v1/customers/update-temperature"


    const val USER_TYPE = "user_type"
    const val TIME_STAMP = "timestamp"
}