package com.jamaica.temperature.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.StringBuilder
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object Commonutils {


    @SuppressLint("SimpleDateFormat")
    fun GetCurrentTime12(): String? {
        val sdf = SimpleDateFormat("hh:mm a")
        var date3=""
        try {
            date3 = sdf.format(Date())

            return date3
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return date3
    }

    fun saveString( pref: SharedPreferences,key:String,value:String){
        var p = pref.edit()
        p.putString(key,value)
        p.commit()
    }

    fun getString(pref: SharedPreferences,key:String) : String? {
        return pref.getString(key,"")
    }

    fun saveBoolean( pref: SharedPreferences,key:String,value:Boolean){
        var p = pref.edit()
        p.putBoolean(key,value)
        p.commit()
    }

    fun getBoolean(pref: SharedPreferences,key:String) : Boolean? {
        return pref.getBoolean(key,false)
    }

    fun saveInteger( pref: SharedPreferences,key:String,value:Int){
        var p = pref.edit()
        p.putInt(key,value)
        p.commit()
    }

    fun getInteger(pref: SharedPreferences,key:String) : Int?{
        return pref.getInt(key,-1)
    }

    // check the json has key and also key object is Json or not if not send
    // empty json
    fun hasJsonForKey(json: JSONObject, key: String): JSONObject {
        try {
            if (json.has(key) && json.get(key) != null && json.get(key) != "") {
                val value = json.get(key)
                if (value is JSONObject) {
                    return value as JSONObject
                }
            }
            return JSONObject()
        } catch (e: JSONException) {
            return JSONObject()
        }

    }

    fun hasStringForKey(json: JSONObject, key: String): String {
        try {
            if (json.has(key) && json.get(key) != null && json.get(key) != "" && json.get(key) != JSONObject.NULL) {
                val value = json.get(key)
                return ("" + value).trim({ it <= ' ' })
            }
            return ""
        } catch (e: JSONException) {
            return ""
        } catch (e: NullPointerException) {
            return ""
        }

    }

    fun hasArrayForKey(json: JSONObject, key: String): JSONArray {
        try {
            if (json.has(key) && json.get(key) != null && json.get(key) != "") {
                val value = json.get(key)
                if (value is JSONArray) {
                    return value as JSONArray
                }
            }
            return JSONArray()
        } catch (e: JSONException) {
            return JSONArray()
        }

    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return if (target == null) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(target!!).matches()
        }
    }

    fun isDataConnected(context: Context): Boolean {
        val mConnectivity =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val info = mConnectivity!!.activeNetworkInfo

        return if (info != null)
            info!!.isConnected
        else
            false
    }

}