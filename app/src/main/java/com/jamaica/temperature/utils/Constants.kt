package com.jamaica.temperature.utils

import android.Manifest.permission.*
import com.jamaica.temperature.ui.data.CountryCodeData
import com.jamaica.temperature.ui.data.HealthHistoryData
import com.jamaica.temperature.ui.data.HistoryData
import com.jamaica.temperature.ui.data.RangeData
import java.security.Policy


object Constants {

    public val PrefName = "TemperaturePref"
    var Authorize = "Authorize"

    val DefAuthorize = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjQzLjg1OjgwMDFcL2FwaVwvdjFcL2N1c3RvbWVyc1wvbG9naW4iLCJpYXQiOjE1ODkxOTE4NzMsImV4cCI6MTYyMDcyNzg3MywibmJmIjoxNTg5MTkxODczLCJqdGkiOiJOa09vYkUwTm44clFMQTdvIiwic3ViIjozLCJwcnYiOiI3ZDA0YzNmNTc2ZWM5YTJjMDhiYzUyYmZlMTk2NjQ3NzdlYWYyYzQ4In0.9ppnlOKaBMZV-tVZ_sXOs69GrCxTbN06qIZ7gJ86bUQ"

    var publicKey:String ="c034f2e921d2b64d1895ec998fd07839"
    const val PRICE_PATTERN = "^[1-9]\\d{0,7}(?:\\.\\d{1,4})?\$"
    val TEXT_REGEX = Regex("[a-zA-Z]")
    val NUMBER_REGEX = Regex("[0-9]+")

    var dirName = "AmberPayMerchant"
    var DEVICE_TYPE = "1"
    var DEVICE_TOKEN = "asas"
    var Policy=""
    var OFFSET: Int = 10

    var CAMERA_PERMISSIONS = arrayOf(
        CAMERA,
        WRITE_EXTERNAL_STORAGE,
        READ_EXTERNAL_STORAGE
    )

    const val ITEM_TYPE_ONE = 0
    const val ITEM_TYPE_TWO = 1

    const val FROM_DATE = "FROM_DATE"
    const val TO_DATE = "TO_DATE"
    val LOGO = "logo"
    val BANNER_IMG = "offer_banner_image"
    const val OFFER_TYPE = "OFFER_TYPE"
    const val OFFER_MAX_value = "OFFER_Max_value"
    const val OFFER_VALUE = "OFFER_VALEUE"
    const val OFFER_NAME = "OFFER_name"
    const val OFFER_DES = "OFFER_Des"
    const val SAVE_IMAGE = "SAVE_IMAGE"
    const val OFFER_CODE = "OFFER_CODE"
    const val EXPIRY_DATE = "EXPIRY_DATE"
    const val OFFER_ID = "OFFER_ID"
    const val SAVE_OFFERS_MODEL = "SAVE_OFFERS_MODEL"
    const val SAVE_DATA = "SAVE_DATA"
    const val is_MOBILE_VERIFY = "is_mobile_number_verify"


    const val CURRENCY_CODE = "Currency_Code"
    const val TEMP_START = "Temp_Start"
    const val TEMP_END  = "Temp_end"
    const val CELSIUS_START = "Celsius_Start"
    const val CELSIUS_END  = "Celsius_end"
    const val HIGH_TEMP = "High_Temp"
    const val HIGH_CELSIUS = "High_Celsius"
    const val LAST_TEMP = "Last_Temp"
    const val LAST_CELSIUS = "Last_Celsius"
    const val LAST_UPDATE_DATE = "Last_Update_Date"

    const val USERID = "UserId"
    const val USERNAME = "UserName"
    const val NAME = "Name"
    const val LASTNAME = "LastName"
    const val MOBILE = "Mobile"
    const val EMAIL = "Email"
    const val ADDRESS = "Address"
    const val GENDER = "Gender"
    const val DOB = "dob"
    const val USERIMAGE = "UserImage"
    const val PATIENTID = "PatientId"
    const val STAFFID = "StaffId"
    const val STATUS = "Status"
    const val CELSIUS_OR_TEMP = "CelisusOrTemp"
    const val OTP = "otp"
    const val DIAL_CODE_ID = "user_dialing_code_id"
    const val OTP_COUNT_MOBILE = "otp_count_mobile_number"
    const val OTP_COUNT_MAIL = "otp_count_email"
    const val OTP_SENT_TO = "otp_sent_to"
    const val MOBILE_VERIFIED_AT = "user_is_mobile_verified_at"
    const val EMAIL_VERIFIED_AT = "is_email_verified_at"

    var countrycodelist = ArrayList<CountryCodeData>()
    var genderlist = ArrayList<CountryCodeData>()
    var symptomlist = ArrayList<CountryCodeData>()
    var rangelist = ArrayList<RangeData>()
    var healthHistoryList = java.util.ArrayList<HealthHistoryData>()
    var HistoryList = java.util.ArrayList<HistoryData>()

}