package com.jamaica.temperature.utils

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.provider.Settings

object NetworkUtils {
    fun isNetWorkAvailable(context: Context): Boolean {
        val connectivityManager: ConnectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected
    }

    fun isInFlightMode(context: Context): Boolean {
        return Settings.Global.getInt(context.contentResolver, Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }
}