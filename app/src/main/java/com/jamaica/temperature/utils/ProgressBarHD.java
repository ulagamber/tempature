package com.jamaica.temperature.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jamaica.temperature.R;


public class ProgressBarHD extends Dialog {

    private Context mContext;
    private ProgressBarHD dialog;

    public ProgressBarHD(Context context){
        super(context);
        mContext = context;
    }

    public ProgressBarHD(Context context, int theme) {
        super(context, theme);
    }

    public ProgressBarHD showDialog(CharSequence message, boolean cancelable, int color) {
        dialog = new ProgressBarHD(mContext, R.style.ProgressHD);
        dialog.setTitle("");
        dialog.setContentView(R.layout.progress_hd);
        ProgressBar b = dialog.findViewById(R.id.progress_img);
        Drawable background = b.getIndeterminateDrawable();
        int color2 = 0xFF00FF00;
        b.getIndeterminateDrawable().setColorFilter(color, android.graphics.PorterDuff.Mode.MULTIPLY);

        TextView txt = (TextView) dialog.findViewById(R.id.load_msg);
        txt.setTextColor(color);
        if (message == null || message.length() == 0) {
            txt.setVisibility(View.GONE);
        } else {
            txt.setVisibility(View.VISIBLE);
            txt.setText(message);
        }

        dialog.setCancelable(cancelable);
//        dialog.setOnCancelListener(cancelListener);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.2f;
        dialog.getWindow().setAttributes(lp);
        // dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.show();
        return dialog;
    }

    public void D_Cancel(){
        dialog.dismiss();
    }
}
